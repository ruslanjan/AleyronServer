/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.security;

import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.services.users.documents.Privilege;
import com.aleyron.services.users.documents.Role;
import com.aleyron.services.users.repositories.AleyronUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class MongoAleyronUserDetailsService implements UserDetailsService {

    private final AleyronUserRepository repository;

    @Autowired
    public MongoAleyronUserDetailsService(AleyronUserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        AleyronUser user;
        user = this.repository.findByUsername(name);
        if (user == null)
            throw new UsernameNotFoundException("username: " + name);
        return new User(user.getUsername(), user.getPassword(),
                getAuthorities(user));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            AleyronUser user) {
        List<GrantedAuthority> collection = getGrantedAuthorities(getPrivilegesFromRoles(user.getRoles()));
        collection.addAll(getGrantedAuthorities(getPrivilegesFromUser(user)));
        return collection;
    }

    private List<String> getPrivilegesFromRoles(Collection<Role> roles) {

        List<String> privileges = new ArrayList<>();
        List<Privilege> collection = new ArrayList<>();
        for (Role role : roles) {
            collection.addAll(role.getPrivileges());
        }
        for (Privilege item : collection) {
            privileges.add(item.getPattern());
        }
        return privileges;
    }

    private List<String> getPrivilegesFromUser(AleyronUser user) {

        List<String> privileges = new ArrayList<>();
        List<Privilege> collection = user.getPrivileges();
        for (Privilege item : collection) {
            privileges.add(item.getPattern());
        }
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}
