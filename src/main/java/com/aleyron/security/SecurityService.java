package com.aleyron.services.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/19/18
 */
@Service
public class SecurityService {
    public boolean hasPattern(User principal, String pattern) {
        for (GrantedAuthority auth : principal.getAuthorities()) {
            Pattern p = Pattern.compile(auth.getAuthority());
            if (p.matcher(pattern).matches())
                return true;
        }
        return false;
    }
}
