package com.aleyron.services.order.controllers;

import com.aleyron.services.order.data.DigitalPrintWorkType;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.services.order.documents.DigitalPrintWork;
import com.aleyron.services.order.logic.OrderService;
import com.aleyron.services.order.logic.PrintWorkService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order/print_work")
public class PrintWorkController {

	private PrintWorkService printWorkService;
	private OrderService orderService;

	@Autowired
	public PrintWorkController(PrintWorkService printWorkService, OrderService orderService) {
		this.printWorkService = printWorkService;
		this.orderService = orderService;
	}

	@PostMapping("/create")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.PrintWork.create')")
	public DigitalPrintWork create(@RequestBody DigitalPrintWork digitalPrintWork) throws Exception {
		return printWorkService.create(digitalPrintWork);
	}

	@GetMapping("/getAllPrintWork")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.PrintWork.getAllPrintWork')")
	public List < DigitalPrintWork > getAllPrintWork( ) {
		return printWorkService.getAllPrintWork();
	}

	@PostMapping("/deletePrintWorkById")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.PrintWork.deletePrintWorkById')")
	public DigitalPrintWork deletePrintWorkById(@RequestBody String id) {
		return printWorkService.deletePrintWorkById(id);
	}

	@GetMapping("/getPrintWorkById")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.PrintWork.getPrintWorkById')")
	public DigitalPrintWork getPrintWorkById(String id) {
		return printWorkService.getPrintWorkById(id);
	}

	@GetMapping("/getPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.PrintWork.getPage')")
	public List < DigitalPrintWork > getPage(Integer page,
	                                         Integer size,
	                                         @RequestParam(value = "sorts[]", required = false)
		                                         String[] sorts,
	                                         @RequestParam(value = "filters[]", required = false)
		                                         String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
		return printWorkService.readPage(page, size, _filters, _sorts);
	}

	@GetMapping("/getAllOrderType")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.PrintWork.getAllOrderType')")
	public List < OrderType > getAllOrderType( ) {
		return orderService.getAllOrderType();
	}

	@GetMapping("/getAllPrintWorkType")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.PrintWork.getAllPrintWorkType')")
	public List < DigitalPrintWorkType > getAllPrintWorkType( ) {
		return printWorkService.getAllPrintWorkType();
	}
}
