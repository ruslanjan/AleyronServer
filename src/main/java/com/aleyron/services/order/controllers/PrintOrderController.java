package com.aleyron.services.order.controllers;

import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.logic.PrintOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/order/print_order")
public class PrintOrderController {
    private PrintOrderService printOrderService;

    @Autowired
    public PrintOrderController(PrintOrderService printOrderService) {
        this.printOrderService = printOrderService;
    }

    @PostMapping("/create")
    @PreAuthorize("@securityService.hasPattern(principal, 'order.PrintOrder.create')")
    public DigitalPrintOrder create(@RequestBody DigitalPrintOrder digitalPrintOrder) {
        return printOrderService.create(digitalPrintOrder);
    }

    @PostMapping("/deleteById")
    @PreAuthorize("@securityService.hasPattern(principal, 'order.PrintOrder.deleteById')")
    public DigitalPrintOrder deleteById(@RequestBody String id) {
        return printOrderService.deleteById(id);
    }
}
