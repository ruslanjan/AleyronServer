package com.aleyron.services.order.controllers;

import com.aleyron.services.booker.logic.BookerService;
import com.aleyron.services.order.documents.DesignOrder;
import com.aleyron.services.order.logic.OrderService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/13/18
 */
@RestController
@RequestMapping("/api/order/design_order")
public class DesignOrderController {

	private OrderService orderService;
	private BookerService bookerService;

	public DesignOrderController(OrderService orderService, BookerService bookerService) {
		this.orderService = orderService;
		this.bookerService = bookerService;
	}

	@PostMapping("/update")
	@PreAuthorize("@securityService.hasPattern(principal, 'order.DesignOrder.update') and " +
		"@orderRepository.existsById(#order.id) and " +
		"@orderRepository.getById(#order.id).invoice != null and " +
		"@orderRepository.getById(#order.id).invoice.status.name() == 'FORMING'")
	public DesignOrder update(@RequestBody DesignOrder order) throws Exception {
		order = (DesignOrder) orderService.writeOrder(order);
		bookerService.writeInvoice(order.getInvoice());
		return order;
	}
}
