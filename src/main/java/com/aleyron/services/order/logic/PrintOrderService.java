package com.aleyron.services.order.logic;


import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.repositories.PrintOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrintOrderService {
    private PrintOrderRepository printOrderRepository;

    @Autowired
    PrintOrderService(PrintOrderRepository printOrderRepository) {
        this.printOrderRepository = printOrderRepository;
    }

    public DigitalPrintOrder create(DigitalPrintOrder digitalPrintWork) {
        return printOrderRepository.save(digitalPrintWork);
    }

    public DigitalPrintOrder deleteById(String id) {
        DigitalPrintOrder digitalPrintOrder = printOrderRepository.getById(id);
        printOrderRepository.deleteById(id);
        return digitalPrintOrder;
    }
}
