package com.aleyron.services.order.logic;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.configs.services.ConfigsService;
import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.services.order.documents.DesignOrder;
import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.documents.Order;
import com.aleyron.services.order.repositories.OrderRepository;
import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/22/18
 */
@Service
public class OrderService {

    private StorageItemService storageService;
    private OrderRepository orderRepository;
    private ConfigsService configsService;
    private PrintWorkService printWorkService;

    @Autowired
    public OrderService(StorageItemService storageService, OrderRepository orderRepository, ConfigsService configsService, PrintWorkService printWorkService) {
        this.storageService = storageService;
        this.orderRepository = orderRepository;
        this.configsService = configsService;
        this.printWorkService = printWorkService;
    }

    public Order getOrderById(String id) {
        return orderRepository.getById(id);
    }

    public List<Order> getPagedOrderByType(OrderType type, int page, int size) {
        return orderRepository.findAllByType(type, PageRequest.of(page, size)).getContent();
    }

    public Order deleteById(String id) throws Exception {
        Order order = getOrderById(id);
        if (order.getInvoice() == null) {
            orderRepository.deleteById(id);
            return order;
        } else {
            throw new Exception("Order is tied to invoice. Untie first");
        }
    }

//	public List<Order> getPagedDesignOrder(int page, int size) {
//		return getPagedOrderByType(OrderType.DESIGN, page, size);
//	}
//
//	public List<Order> getPagedOrders(int page, int size) {
//		return orderRepository.findAll(PageRequest.of(page, size)).getContent();
//	}
//
//	public List<Order> getPagedWideFormatPrintOrder(int page, int size) {
//		return getPagedOrderByType(OrderType.WIDE_FORMAT_PRINT, page, size);
//	}

    public List<Order> getPagedOrderByInvoice_Status(InvoiceStatus status, int page, int size) {
        return orderRepository.findAllByInvoice_Status(status, PageRequest.of(page, size)).getContent();
    }

    public Order writeOrder(Order order) {
        return orderRepository.save(order);
    }

	public long designOrderPrice(DesignOrder order) {
        return order.getCost();
    }

    public long digitalPrintOrderPrice(DigitalPrintOrder order) {
        return order.calculateCost(storageService, configsService);
    }

    public List<OrderType> getAllOrderType() {
        return Arrays.asList(OrderType.values());
    }

    public List<Order> readPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
        FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);

        return orderRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
    }

    public List<DesignOrder> getPageDesignOrderByUser(AleyronUser user, int page, int size) {
        return (List<DesignOrder>) (Object) orderRepository.getDesignOrderByUserPage(user, PageRequest.of(page, size)).getContent();
    }
}
