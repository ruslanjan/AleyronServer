package com.aleyron.services.order.logic;

import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.order.data.DigitalPrintWorkStorageItem;
import com.aleyron.services.order.data.DigitalPrintWorkType;
import com.aleyron.services.order.documents.DigitalPrintWork;
import com.aleyron.services.order.repositories.PrintWorkRepository;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class PrintWorkService {
	private PrintWorkRepository printWorkRepository;
	private StorageItemService storageItemService;

	@Autowired
	PrintWorkService(PrintWorkRepository printWorkRepository, StorageItemService storageItemService) {
		this.printWorkRepository = printWorkRepository;
		this.storageItemService = storageItemService;
	}

	List < DigitalPrintWork > getAll( ) {

		return null;
	}

	public DigitalPrintWork loadStorageItems(DigitalPrintWork work) {
		for (DigitalPrintWorkStorageItem item : work.getBlockMaterials())
			item.setItem(storageItemService.readItem(item.getId()));
		for (DigitalPrintWorkStorageItem item : work.getSquareMeterMaterials())
			item.setItem(storageItemService.readItem(item.getId()));
		for (DigitalPrintWorkStorageItem item : work.getPerimeterMaterials())
			item.setItem(storageItemService.readItem(item.getId()));
		for (DigitalPrintWorkStorageItem item : work.getOneTimeMaterials())
			item.setItem(storageItemService.readItem(item.getId()));
		return work;
	}

	public List < DigitalPrintWork > loadStorageItems(List < DigitalPrintWork > works) {
		for (DigitalPrintWork work : works)
			loadStorageItems(work);
		return works;
	}

	public List < DigitalPrintWork > getAllPrintWork( ) {
		List < DigitalPrintWork > works = printWorkRepository.findAll();
		loadStorageItems(works);
		return works;
	}


	public DigitalPrintWork deletePrintWorkById(String id) {
		DigitalPrintWork digitalPrintWork = printWorkRepository.getById(id);
		loadStorageItems(digitalPrintWork);
		printWorkRepository.deleteById(id);
		return digitalPrintWork;
	}


	public DigitalPrintWork getPrintWorkById(String id) {
		return loadStorageItems(printWorkRepository.getById(id));
	}

	public List < DigitalPrintWork > readPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);

		return printWorkRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}

	public DigitalPrintWork create(DigitalPrintWork digitalPrintWork) throws Exception {
		if (digitalPrintWork.getBlockMaterials().contains(null) ||
			digitalPrintWork.getOneTimeMaterials().contains(null) ||
			digitalPrintWork.getPerimeterMaterials().contains(null) ||
			digitalPrintWork.getSquareMeterMaterials().contains(null))
			throw new Exception("null Material");
		return loadStorageItems(printWorkRepository.save(digitalPrintWork));
	}


	public List < DigitalPrintWorkType > getAllPrintWorkType( ) {
		return Arrays.asList(DigitalPrintWorkType.values());
	}
}
