package com.aleyron.services.order.repositories;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.services.order.documents.Order;
import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Map;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/23/18
 */
public interface OrderRepository extends MongoRepository < Order, String > {
	Order getById(String id);

	Page < Order > findAllByType(OrderType type, Pageable pageable);

	Page < Order > findAllByInvoice_Status(InvoiceStatus status, Pageable pageable);

	@Query("?0")
	Page < Order > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

	@Query("{ 'type': 'DESIGN', 'designer': '?0'}")
	Page < Order > getDesignOrderByUserPage(AleyronUser user, Pageable pageable);

	void deleteById(String id);
}
