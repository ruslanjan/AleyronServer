package com.aleyron.services.order.repositories;

import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Map;

public interface PrintOrderRepository extends MongoRepository < DigitalPrintOrder, String > {
	DigitalPrintOrder getById(String id);

	@Query("?0")
	Page < DigitalPrintOrder > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);


}
