package com.aleyron.services.order.documents;

import com.aleyron.services.configs.services.ConfigsService;
import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.order.data.DigitalPrintWorkStorageItem;
import com.aleyron.services.order.data.DigitalPrintWorkType;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.services.order.data.Rational;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/22/18
 */
@Document
public class DigitalPrintWork {
    @Id
    private String id;
    private String name;
    private Rational blockSize = new Rational(1L, 1L); // dividend, divisor
    private double blockPrice;
    private double squareMeterPrice;
    private double oneTimePrice;
    private double perimeterPrice;
    private long estimatedTime;
    private long cost;

    private DigitalPrintWorkType type;
    private List<OrderType> orderTypes = new ArrayList<>();
    private List<DigitalPrintWorkStorageItem> blockMaterials = new ArrayList<>();
    private List<DigitalPrintWorkStorageItem> squareMeterMaterials = new ArrayList<>();
    private List<DigitalPrintWorkStorageItem> oneTimeMaterials = new ArrayList<>();
    private List<DigitalPrintWorkStorageItem> perimeterMaterials = new ArrayList<>();

    public long calculateCost(double height, double width, double count, StorageItemService storageItemService, ConfigsService configsService) {
        long price = 0;
        //oneTimePrice
        price += Math.ceil(oneTimePrice);
        for (DigitalPrintWorkStorageItem item : oneTimeMaterials) {
            price += Math.ceil(storageItemService.readItem(item.getId()).getCost() * item.getQuantity());
        }
        //blockPrice
        price += Math.ceil(count / blockSize.getDividend() * blockSize.getDivisor() * blockPrice);
        for (DigitalPrintWorkStorageItem item : blockMaterials) {
            price += Math.ceil(count / blockSize.getDividend() * blockSize.getDivisor() * storageItemService.readItem(item.getId()).getCost() * item.getQuantity());
        }
        //squareMeterPrice
        price += Math.ceil(height * width * count * squareMeterPrice);
        for (DigitalPrintWorkStorageItem item : squareMeterMaterials) {
            price += Math.ceil(height * width * count * storageItemService.readItem(item.getId()).getCost() * item.getQuantity());
        }
        //perimeterPrice
        price += Math.ceil((height + width) * count * perimeterPrice);
        for (DigitalPrintWorkStorageItem item : perimeterMaterials) {
            price += Math.ceil((height + width) * count * storageItemService.readItem(item.getId()).getCost() * item.getQuantity());
        }
        this.cost = price;
        return price;
    }

    public String getId() {
        return id;
    }

    public DigitalPrintWork setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public DigitalPrintWork setName(String name) {
        this.name = name;
        return this;
    }

    public Rational getBlockSize() {
        return blockSize;
    }

    public DigitalPrintWork setBlockSize(Rational blockSize) {
        this.blockSize = blockSize;
        return this;
    }

    public double getBlockPrice() {
        return blockPrice;
    }

    public DigitalPrintWork setBlockPrice(double blockPrice) {
        this.blockPrice = blockPrice;
        return this;
    }

    public double getSquareMeterPrice() {
        return squareMeterPrice;
    }

    public DigitalPrintWork setSquareMeterPrice(double squareMeterPrice) {
        this.squareMeterPrice = squareMeterPrice;
        return this;
    }

    public double getOneTimePrice() {
        return oneTimePrice;
    }

    public DigitalPrintWork setOneTimePrice(double oneTimePrice) {
        this.oneTimePrice = oneTimePrice;
        return this;
    }

    public double getPerimeterPrice() {
        return perimeterPrice;
    }

    public DigitalPrintWork setPerimeterPrice(double perimeterPrice) {
        this.perimeterPrice = perimeterPrice;
        return this;
    }

    public long getEstimatedTime() {
        return estimatedTime;
    }

    public DigitalPrintWork setEstimatedTime(long estimatedTime) {
        this.estimatedTime = estimatedTime;
        return this;
    }

    public long getCost() {
        return cost;
    }

    public DigitalPrintWork setCost(long cost) {
        this.cost = cost;
        return this;
    }

    public DigitalPrintWorkType getType() {
        return type;
    }

    public DigitalPrintWork setType(DigitalPrintWorkType type) {
        this.type = type;
        return this;
    }

    public List<OrderType> getOrderTypes() {
        return orderTypes;
    }

    public DigitalPrintWork setOrderTypes(List<OrderType> orderTypes) {
        this.orderTypes = orderTypes;
        return this;
    }

    public List<DigitalPrintWorkStorageItem> getBlockMaterials() {
        return blockMaterials;
    }

    public DigitalPrintWork setBlockMaterials(List<DigitalPrintWorkStorageItem> blockMaterials) {
        this.blockMaterials = blockMaterials;
        return this;
    }

    public List<DigitalPrintWorkStorageItem> getSquareMeterMaterials() {
        return squareMeterMaterials;
    }

    public DigitalPrintWork setSquareMeterMaterials(List<DigitalPrintWorkStorageItem> squareMeterMaterials) {
        this.squareMeterMaterials = squareMeterMaterials;
        return this;
    }

    public List<DigitalPrintWorkStorageItem> getOneTimeMaterials() {
        return oneTimeMaterials;
    }

    public DigitalPrintWork setOneTimeMaterials(List<DigitalPrintWorkStorageItem> oneTimeMaterials) {
        this.oneTimeMaterials = oneTimeMaterials;
        return this;
    }

    public List<DigitalPrintWorkStorageItem> getPerimeterMaterials() {
        return perimeterMaterials;
    }

    public DigitalPrintWork setPerimeterMaterials(List<DigitalPrintWorkStorageItem> perimeterMaterials) {
        this.perimeterMaterials = perimeterMaterials;
        return this;
    }
}
