package com.aleyron.services.order.documents;

import com.aleyron.services.booker.documents.Invoice;
import com.aleyron.services.order.data.OrderType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/22/18
 */
@Document
public class Order {
    @Id
    private String id;
    @DBRef(lazy = true)
    private Invoice invoice;
    private OrderType type;
    private long estimatedTime;
    private Date created = new Date();
    private Date finished;
    private long cost;

    public String getId() {
        return id;
    }

    public Order setId(String id) {
        this.id = id;
        return this;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public Order setInvoice(Invoice invoice) {
        this.invoice = invoice;
        return this;
    }

    public OrderType getType() {
        return type;
    }

    public Order setType(OrderType type) {
        this.type = type;
        return this;
    }

    public long getEstimatedTime() {
        return estimatedTime;
    }

    public Order setEstimatedTime(long estimatedTime) {
        this.estimatedTime = estimatedTime;
        return this;
    }

    public Date getCreated() {
        return created;
    }

    public Order setCreated(Date created) {
        this.created = created;
        return this;
    }

    public Date getFinished() {
        return finished;
    }

    public Order setFinished(Date finished) {
        this.finished = finished;
        return this;
    }

    public long getCost() {
        return cost;
    }

    public Order setCost(long cost) {
        //if (invoice != null && invoice.getStatus() == InvoiceStatus.FORMING)
        this.cost = cost;
        return this;
    }
}
