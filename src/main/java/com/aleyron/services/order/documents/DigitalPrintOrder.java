package com.aleyron.services.order.documents;

import com.aleyron.services.configs.documents.PriceCalculationConfig;
import com.aleyron.services.configs.services.ConfigsService;
import com.aleyron.services.digital_print.services.storage.documents.StorageItem;
import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.services.order.data.PrintOrderStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/22/18
 */
public class DigitalPrintOrder extends Order {
    private PrintOrderStatus status = PrintOrderStatus.WAITING;

    private String design;
    private long count;
    private double height;
    private double width;
    private double techCornersHeight = 0.01;
    private double techCornersWidth = 0.01;

    private String comments;

    private StorageItem printMaterial;
    private List<DigitalPrintWork> preDigitalPrintWorks = new ArrayList<>();
    private List<DigitalPrintWork> postDigitalPrintWorks = new ArrayList<>();

    public DigitalPrintOrder() {
        setType(OrderType.DIGITAL_PRINT);
    }

    public long calculateCost(StorageItemService storageService, ConfigsService configsService) {
        PriceCalculationConfig calculationConfig = configsService.getPriceCalculationConfig();

        long price = 0;
        price += Math.ceil((height + techCornersHeight) * (width + techCornersHeight) * count * calculationConfig.getAdditionPricePerSquareMeter().get(getType()));
        //Print Material cost
        price += Math.ceil((height + techCornersHeight) * (width + techCornersWidth) * count * storageService.readItem(printMaterial.getId()).getCost());

        //Print works cost
		for (DigitalPrintWork work : preDigitalPrintWorks)
			price += work.calculateCost(height + techCornersHeight, count,
					width + techCornersWidth, storageService, configsService);
		for (DigitalPrintWork work : postDigitalPrintWorks)
			price += work.calculateCost(height + techCornersHeight, count,
					width + techCornersWidth, storageService, configsService);
		//Saving the cost
        setCost(price);
        //Returning the cost
        return price;
    }

    public String getDesign() {
        return design;
    }

    public DigitalPrintOrder setDesign(String design) {
        this.design = design;
        return this;
    }

    public long getCount() {
        return count;
    }

    public DigitalPrintOrder setCount(long count) {
        this.count = count;
        return this;
    }

    public PrintOrderStatus getStatus() {
        return status;
    }

    public DigitalPrintOrder setStatus(PrintOrderStatus status) {
        this.status = status;
        return this;
    }

    public List<DigitalPrintWork> getPreDigitalPrintWorks() {
        return preDigitalPrintWorks;
    }

    public DigitalPrintOrder setPreDigitalPrintWorks(List<DigitalPrintWork> preDigitalPrintWorks) {
        this.preDigitalPrintWorks = preDigitalPrintWorks;
        return this;
    }

    public List<DigitalPrintWork> getPostDigitalPrintWorks() {
        return postDigitalPrintWorks;
    }

    public DigitalPrintOrder setPostDigitalPrintWorks(List<DigitalPrintWork> postDigitalPrintWorks) {
        this.postDigitalPrintWorks = postDigitalPrintWorks;
        return this;
    }

    public double getHeight() {
        return height;
    }

    public DigitalPrintOrder setHeight(double height) {
        this.height = height;
        return this;
    }

    public double getWidth() {
        return width;
    }

    public DigitalPrintOrder setWidth(double width) {
        this.width = width;
        return this;
    }

    public double getTechCornersHeight() {
        return techCornersHeight;
    }

    public DigitalPrintOrder setTechCornersHeight(double techCornersHeight) {
        this.techCornersHeight = techCornersHeight;
        return this;
    }

    public double getTechCornersWidth() {
        return techCornersWidth;
    }

    public DigitalPrintOrder setTechCornersWidth(double techCornersWidth) {
        this.techCornersWidth = techCornersWidth;
        return this;
    }

    public String getComments() {
        return comments;
    }

    public DigitalPrintOrder setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public StorageItem getPrintMaterial() {
        return printMaterial;
    }

    public DigitalPrintOrder setPrintMaterial(StorageItem printMaterial) {
        this.printMaterial = printMaterial;
        return this;
    }
}
