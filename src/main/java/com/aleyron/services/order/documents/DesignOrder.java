package com.aleyron.services.order.documents;

import com.aleyron.services.order.data.OrderType;
import com.aleyron.services.users.documents.AleyronUser;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/22/18
 */

public class DesignOrder extends Order {

    @Indexed(sparse = true)
    private AleyronUser designer;

    public DesignOrder() {
        setType(OrderType.DESIGN);
    }

    public AleyronUser getDesigner() {
        return designer;
    }

    public DesignOrder setDesigner(AleyronUser designer) {
        this.designer = designer;
        return this;
    }
}
