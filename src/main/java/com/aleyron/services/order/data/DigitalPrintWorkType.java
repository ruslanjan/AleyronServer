package com.aleyron.services.order.data;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/28/18
 */
public enum DigitalPrintWorkType {
    POST, PRE
}
