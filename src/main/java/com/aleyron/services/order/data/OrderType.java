package com.aleyron.services.order.data;

public enum OrderType {
    STICKER, WIDE_FORMAT_PRINT, DESIGN, DIGITAL_PRINT
}
