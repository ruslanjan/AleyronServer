package com.aleyron.services.order.data;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/20/18
 */
public class Rational {
    private long dividend = 1;
    private long divisor = 1;

    Rational() {
    }

    public Rational(long dividend, long divisor) {
        this.dividend = dividend;
        this.divisor = divisor;
    }

    public double getDouble() {
        return dividend * 1.0 / divisor;
    }

    public long getDividend() {
        return dividend;
    }

    public Rational setDividend(long dividend) {
        this.dividend = dividend;
        return this;
    }

    public long getDivisor() {
        return divisor;
    }

    public Rational setDivisor(long divisor) {
        this.divisor = divisor;
        return this;
    }
}
