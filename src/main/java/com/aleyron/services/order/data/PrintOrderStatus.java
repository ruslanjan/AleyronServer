package com.aleyron.services.order.data;

public enum PrintOrderStatus {
    WAITING, PICKING_UP_MATERIALS, PROCESSING, PRE_PRINT_WORK, PRINT, POST_PRINT_WORK, FINISHED, PAUSED, FAILURE
}
