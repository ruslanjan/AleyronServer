package com.aleyron.services.order.data;

import com.aleyron.services.digital_print.services.storage.documents.StorageItem;
import org.springframework.data.annotation.Transient;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/10/18
 */
public class DigitalPrintWorkStorageItem {
    private String id;
    private double quantity;
    @Transient
    private StorageItem item = null;

    public String getId() {
        return id;
    }

    public DigitalPrintWorkStorageItem setId(String id) {
        this.id = id;
        return this;
    }

    public double getQuantity() {
        return quantity;
    }

    public DigitalPrintWorkStorageItem setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    public StorageItem getItem() {
        return item;
    }

    public DigitalPrintWorkStorageItem setItem(StorageItem item) {
        this.item = item;
        return this;
    }
}
