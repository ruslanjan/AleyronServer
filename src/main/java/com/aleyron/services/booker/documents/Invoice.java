package com.aleyron.services.booker.documents;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.order.documents.Order;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/20/18
 */
@Document
public class Invoice {
    @Id
    private String id;

    @TextIndexed
    private String clientName;
    private String title;
    private String manager;
    private String invoice1C;
    private String clientEmail;
    private String clientPhoneNumber;
    private String clientDescription;
    private Date dateCreated;
    private InvoiceStatus status = InvoiceStatus.FORMING;

    private long cost;
    @Transient
    private List<Order> orders = new ArrayList<>();
    private List<String> orderIds = new ArrayList<>();

    public List<Order> getOrders() {
        return orders;
    }

    public Invoice setOrders(List<Order> orders) {
        this.orders = orders;
        return this;
    }

    public String getId() {
        return id;
    }

    public Invoice setId(String id) {
        this.id = id;
        return this;
    }

    public long getCost() {
        return this.cost;
    }

    public Invoice setCost(long cost) {
        this.cost = cost;
        return this;
    }

    public String getClientName() {
        return clientName;
    }

    public Invoice setClientName(String clientName) {
        this.clientName = clientName;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Invoice setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getManager() {
        return manager;
    }

    public Invoice setManager(String manager) {
        this.manager = manager;
        return this;
    }

    public String getInvoice1C() {
        return invoice1C;
    }

    public Invoice setInvoice1C(String invoice1C) {
        this.invoice1C = invoice1C;
        return this;
    }

    public InvoiceStatus getStatus() {
        return status;
    }

    public Invoice setStatus(InvoiceStatus status) {
        this.status = status;
        return this;
    }

    public List<String> getOrderIds() {
        return orderIds;
    }

    public Invoice setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
        return this;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Invoice setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public Invoice setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
        return this;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public Invoice setClientPhoneNumber(String clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;
        return this;
    }

    public String getClientDescription() {
        return clientDescription;
    }

    public Invoice setClientDescription(String clientDescription) {
        this.clientDescription = clientDescription;
        return this;
    }
}
