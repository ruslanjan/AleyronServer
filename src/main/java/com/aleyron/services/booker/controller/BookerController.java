package com.aleyron.services.booker.controller;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.booker.documents.Invoice;
import com.aleyron.services.booker.logic.BookerService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/17/18
 */

@RestController
@RequestMapping("/api/booker/invoices")
public class BookerController {

	private BookerService bookerService;

	@Autowired
	public BookerController(BookerService bookerService) {
		this.bookerService = bookerService;
	}

	@GetMapping("/getPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'booker.getPage')")
	public List < Invoice > getPage(Integer page,
	                                Integer size,
	                                @RequestParam(value = "sorts[]", required = false)
		                                String[] sorts,
	                                @RequestParam(value = "filters[]", required = false)
		                                String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
		return bookerService.getInvoicePage(page, size, _filters, _sorts);
	}

	@GetMapping("/getById")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.get')")
	public Invoice getInvoiceById(String id) {
		return bookerService.getInvoiceById(id);
	}

	@PostMapping("/status/setPaid")
	@PreAuthorize("@securityService.hasPattern(principal, 'booker.setPaid')")
	public void setPaid(@RequestBody String id) {
		bookerService.setInvoiceStatusPaid(id);
	}

	@PostMapping("/status/setCancelled")
	@PreAuthorize("@securityService.hasPattern(principal, 'booker.setCancelled')")
	public void setCancelled(@RequestBody String id) {
		bookerService.setInvoiceStatusCancelled(id);
	}

	@PostMapping("/status/setPending")
	@PreAuthorize("@securityService.hasPattern(principal, 'booker.setPending')")
	public void setPending(@RequestBody String id) {
		bookerService.setInvoiceStatusPending(id);
	}

	@PostMapping("/status/set")
	@PreAuthorize("@securityService.hasPattern(principal, 'booker.setInvoiceStatus')")
	public void setInvoiceStatus(String id, InvoiceStatus status) {
		bookerService.setInvoiceStatus(id, status);
	}
}
