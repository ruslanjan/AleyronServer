package com.aleyron.services.booker.repositories;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.booker.documents.Invoice;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Map;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/21/18
 */
public interface InvoiceRepository extends MongoRepository < Invoice, String > {
	Invoice getById(String id);

	List < Invoice > getAllByStatus(InvoiceStatus status);

	Invoice getByInvoice1C(String invoice_1c);

	@Query("?0")
	Page < Invoice > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

	boolean existsById(String id);
}
