package com.aleyron.services.booker.data;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/22/18
 */
public enum InvoiceStatus {
    FORMING, CANCELLED, PENDING, PAID
}
