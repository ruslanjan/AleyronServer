package com.aleyron.services.booker.logic;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.booker.documents.Invoice;
import com.aleyron.services.booker.repositories.InvoiceRepository;
import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.order.documents.Order;
import com.aleyron.services.order.logic.OrderService;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/17/18
 */
@Service
public class BookerService {
	private StorageItemService storageService;
	private InvoiceRepository invoiceRepository;
	private OrderService orderService;

	@Autowired
	public BookerService(StorageItemService storageService, InvoiceRepository invoiceRepository, OrderService orderService) {
		this.orderService = orderService;
		this.storageService = storageService;
		this.invoiceRepository = invoiceRepository;
	}

	public void setInvoiceStatusPaid(String id) {
		setInvoiceStatus(id, InvoiceStatus.PAID);
	}

	public void setInvoiceStatusCancelled(String id) {
		setInvoiceStatus(id, InvoiceStatus.CANCELLED);
	}

	public void setInvoiceStatusPending(String id) {
		setInvoiceStatus(id, InvoiceStatus.PENDING);
	}

	public Invoice setInvoiceStatus(String id, InvoiceStatus status) {
		return invoiceRepository.save(invoiceRepository.getById(id).setStatus(status));
	}

	public Invoice writeInvoice(Invoice invoice) throws Exception {
		if (invoice.getId() != null && invoiceRepository.getById(invoice.getId()).getStatus() != InvoiceStatus.FORMING) {
			throw new Exception("Invoice is immutable");
		}
		invoice.setCost(calculateCost(invoice));
		return invoiceRepository.save(invoice);
	}

	@Deprecated
	public Invoice createInvoice(List < Order > printOrders) {
		Invoice invoice = new Invoice();
		invoice.setOrders(printOrders);
		invoice.setCost(calculateCost(invoice));
		return invoiceRepository.save(invoice);
	}

	public Invoice getInvoiceById(String id) {
		Invoice invoice = invoiceRepository.getById(id);
		loadOrdersInInvoice(invoice);
		return invoice;
	}

	private long calculateCost(Invoice invoice) {
		long cost = 0;
		//calculate cost for  All orders
		for (String orderId : invoice.getOrderIds()) {
			Order order = orderService.getOrderById(orderId);
			cost += order.getCost();
		}
		return cost;
	}

	public Invoice getInvoiceBy1c(String id) {
		return invoiceRepository.getByInvoice1C(id);
	}

	private Invoice loadOrdersInInvoice(Invoice invoice) {
		invoice.setOrders(new ArrayList <>());
		for (String orderId : invoice.getOrderIds()) {
			invoice.getOrders().add(orderService.getOrderById(orderId));
		}
		return invoice;
	}

	public List < Invoice > getInvoicePage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);
		return invoiceRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}

	public boolean existsById(String id) {
		return invoiceRepository.existsById(id);
	}
}
