package com.aleyron.services.manager.logic;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.booker.documents.Invoice;
import com.aleyron.services.booker.logic.BookerService;
import com.aleyron.services.order.documents.DesignOrder;
import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.documents.Order;
import com.aleyron.services.order.logic.OrderService;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/23/18
 */
@Service
public class ManagerService {
	private BookerService bookerService;
	private OrderService orderService;


	@Autowired
	public ManagerService(BookerService bookerService, OrderService orderService) {
		this.bookerService = bookerService;
		this.orderService = orderService;
	}

	public Invoice createInvoice(Invoice invoice) throws Exception {
		return bookerService.writeInvoice(invoice);
	}

	@Deprecated
	public Invoice addOrderToInvoice(String invoiceId, Order order) throws Exception {
		if (getInvoiceById(invoiceId).getStatus() != InvoiceStatus.FORMING)
			throw new Exception("Invoice is formed");
		Invoice invoice = bookerService.getInvoiceById(invoiceId);
		List < String > orderIds = invoice.getOrderIds();
		orderIds.add(order.getId());
		bookerService.writeInvoice(invoice);
		order.setInvoice(invoice);
		orderService.writeOrder(order);
		return invoice;
	}

	public Invoice getInvoiceById(String id) {
		return bookerService.getInvoiceById(id);
	}

	public Invoice setStatus(String id, InvoiceStatus status) throws Exception {
		if (getInvoiceById(id).getStatus() != InvoiceStatus.FORMING)
			throw new Exception("Invoice is formed");
		return bookerService.setInvoiceStatus(id, status);
	}

	public Invoice addDigitalPrintOrderToInvoice(String invoiceId, DigitalPrintOrder order) throws Exception {
		if (getInvoiceById(invoiceId).getStatus() != InvoiceStatus.FORMING)
			throw new Exception("Invoice is formed");
		Invoice invoice = bookerService.getInvoiceById(invoiceId);
		order.setInvoice(invoice);
		orderService.digitalPrintOrderPrice(order);
		order = (DigitalPrintOrder) orderService.writeOrder(order);

		List < String > orderIds = invoice.getOrderIds();
		orderIds.add(order.getId());

		invoice.setOrderIds(orderIds);
		bookerService.writeInvoice(invoice);

		return invoice;
	}

	public Invoice addDesignOrderToInvoice(String invoiceId, DesignOrder order) throws Exception {
		if (getInvoiceById(invoiceId).getStatus() != InvoiceStatus.FORMING)
			throw new Exception("Invoice is formed");
		Invoice invoice = bookerService.getInvoiceById(invoiceId);
		order.setInvoice(invoice);
		orderService.designOrderPrice(order);
		order = (DesignOrder) orderService.writeOrder(order);

		List < String > orderIds = invoice.getOrderIds();
		orderIds.add(order.getId());

		invoice.setOrderIds(orderIds);
		bookerService.writeInvoice(invoice);

		return invoice;
	}

	public Invoice getInvoiceBy1c(String id) {
		return bookerService.getInvoiceBy1c(id);
	}

	public List < Invoice > readPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		return bookerService.getInvoicePage(page, size, filters, sorts);
	}

	public Order deleteOrderById(String id) throws Exception {
		Order order = orderService.getOrderById(id);
		Invoice invoice = order.getInvoice();
		invoice.getOrderIds().remove(order.getId());
		bookerService.writeInvoice(invoice);
		order.setInvoice(null);
		orderService.writeOrder(order);
		orderService.deleteById(order.getId());
		return order;
	}
}
