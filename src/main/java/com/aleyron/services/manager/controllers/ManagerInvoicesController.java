package com.aleyron.services.manager.controllers;

import com.aleyron.services.booker.data.InvoiceStatus;
import com.aleyron.services.booker.documents.Invoice;
import com.aleyron.services.manager.data.IdAndStatus;
import com.aleyron.services.manager.data.InvoiceIdAndOrder;
import com.aleyron.services.manager.logic.ManagerService;
import com.aleyron.services.order.documents.DesignOrder;
import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/manager/invoices")
public class ManagerInvoicesController {
	private ManagerService managerService;


	@Autowired
	public ManagerInvoicesController(ManagerService managerService) {
		this.managerService = managerService;
	}

	@PostMapping("/cancel")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.cancel')")
	public Invoice cancel(@RequestBody String id) throws Exception {
		return managerService.setStatus(id, InvoiceStatus.CANCELLED);
	}

	@PostMapping("/form")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.form')")
	public Invoice form(@RequestBody String id) throws Exception {
		return managerService.setStatus(id, InvoiceStatus.PENDING);
	}

	@PostMapping("/create")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.create')" +
		"and #invoice.id == null ")
	public Invoice create(@RequestBody Invoice invoice) throws Exception {
		invoice.setDateCreated(new Date());
		return managerService.createInvoice(invoice);
	}

	@PostMapping("/update")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.update') and " +
		"@bookerService.getInvoiceById(#invoice.id).status.name() == 'FORMING'")
	public Invoice update(@RequestBody Invoice invoice) throws Exception {
		return managerService.createInvoice(invoice);
	}

	@PostMapping("/addDigitalPrintOrder")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.addDigitalPrintOrder') and " +
		"@bookerService.getInvoiceById(#invoiceAndOrder.invoiceId).status.name() == 'FORMING'")
	public Invoice addDigitalPrintOrder(@RequestBody InvoiceIdAndOrder < DigitalPrintOrder > invoiceAndOrder) throws Exception {
		return managerService.addDigitalPrintOrderToInvoice(invoiceAndOrder.getInvoiceId(), invoiceAndOrder.getOrder());
	}

	@PostMapping("/addDesignOrder")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.addDesignOrder') and " +
		"@bookerService.getInvoiceById(#invoiceAndOrder.invoiceId).status.name() == 'FORMING'")
	public Invoice addDesignOrder(@RequestBody InvoiceIdAndOrder < DesignOrder > invoiceAndOrder) throws Exception {
		return managerService.addDesignOrderToInvoice(invoiceAndOrder.getInvoiceId(), invoiceAndOrder.getOrder());
	}

	@GetMapping("/getPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.getPage')")
	public List < Invoice > getPage(Integer page,
	                                Integer size,
	                                @RequestParam(value = "sorts[]", required = false)
		                                String[] sorts,
	                                @RequestParam(value = "filters[]", required = false)
		                                String[] filters
	) {

		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
		return managerService.readPage(page, size, _filters, _sorts);
	}

	@GetMapping("/getById")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.getById') ")
	public Invoice getById(String id) {
		return managerService.getInvoiceById(id);
	}

	@GetMapping("/getBy1c")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.getBy1c') ")
	public Invoice getBy1c(String id) {
		return managerService.getInvoiceBy1c(id);
	}

	@PostMapping("/setStatus")
	@PreAuthorize("@securityService.hasPattern(principal, 'manager.invoices.setStatus') ")
	public Invoice setStatus(@RequestBody IdAndStatus idAndStatus) throws Exception {
		return managerService.setStatus(idAndStatus.getId(), idAndStatus.getStatus());
	}
}
