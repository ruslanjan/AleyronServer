package com.aleyron.services.manager.controllers;

import com.aleyron.services.booker.logic.BookerService;
import com.aleyron.services.configs.services.ConfigsService;
import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.manager.data.DigitalPrintOrderAndDigitalPrintWork;
import com.aleyron.services.manager.logic.ManagerService;
import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.documents.DigitalPrintWork;
import com.aleyron.services.order.documents.Order;
import com.aleyron.services.order.logic.OrderService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/manager/orders")
public class ManagerOrderController {
    private BookerService bookerService;
    private OrderService orderService;
    private ManagerService managerService;
    private StorageItemService storageItemService;
    private ConfigsService configsService;


    @Autowired
    public ManagerOrderController(BookerService bookerService, OrderService orderService, ManagerService managerService, StorageItemService storageItemService, ConfigsService configsService) {
        this.bookerService = bookerService;
        this.orderService = orderService;
        this.managerService = managerService;
        this.storageItemService = storageItemService;
        this.configsService = configsService;
    }

    @GetMapping("/getById")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.orders.getById') ")
    public Order getById(String id) {
        return orderService.getOrderById(id);
    }

    @GetMapping("/getByIds")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.orders.getById') ")
    public List<Order> getById(List<String> ids) {
        List<Order> orders = new ArrayList<>();

        for (String id :
            ids) {
            orders.add(orderService.getOrderById(id));
        }

        return orders;
    }

    @GetMapping("/getPage")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.orders.getPage') ")
    public List<Order> getPage(Integer page,
	                             Integer size,
	                             @RequestParam(value = "sorts[]", required = false)
		                                 String[] sorts,
	                             @RequestParam(value = "filters[]", required = false)
		                                 String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
        return orderService.readPage(page, size, _filters, _sorts);
    }


    @PostMapping("/deleteById")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.orders.deleteById') and " +
        "@orderService.getOrderById(#id).invoice.status.name() == 'FORMING'")
    public Order deleteById(@RequestBody String id) throws Exception {
        return managerService.deleteOrderById(id);
    }

    @PostMapping("/calculatePrintWorkPrice")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.orders.calculatePrintWorkPrice') ")
    public long calculatePrintWorkPrice(@RequestBody DigitalPrintOrderAndDigitalPrintWork digitalPrintOrderAndDigitalPrintWork) {
        DigitalPrintWork work = digitalPrintOrderAndDigitalPrintWork.getWork();
        DigitalPrintOrder order = digitalPrintOrderAndDigitalPrintWork.getOrder();
        return work.calculateCost(order.getHeight(), order.getWidth(), order.getCount(),
                storageItemService, configsService);
    }

    @PostMapping("/calculatePrintOrderPrice")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.orders.calculatePrintOrderPrice') ")
    public long calculatePrintOrderPrice(@RequestBody DigitalPrintOrder order) {
        return order.calculateCost(storageItemService, configsService);
    }
}
