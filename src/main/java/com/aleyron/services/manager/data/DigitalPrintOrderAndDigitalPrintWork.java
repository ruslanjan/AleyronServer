package com.aleyron.services.manager.data;

import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.documents.DigitalPrintWork;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/26/18
 */
public class DigitalPrintOrderAndDigitalPrintWork {
    private DigitalPrintOrder order;
    private DigitalPrintWork work;

    public DigitalPrintOrder getOrder() {
        return order;
    }

    public DigitalPrintOrderAndDigitalPrintWork setOrder(DigitalPrintOrder order) {
        this.order = order;
        return this;
    }

    public DigitalPrintWork getWork() {
        return work;
    }

    public DigitalPrintOrderAndDigitalPrintWork setWork(DigitalPrintWork work) {
        this.work = work;
        return this;
    }
}
