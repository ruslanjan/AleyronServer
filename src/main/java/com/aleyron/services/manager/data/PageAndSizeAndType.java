package com.aleyron.services.manager.data;

import com.aleyron.services.order.data.OrderType;

public class PageAndSizeAndType {
    private Integer page;
    private Integer size;
    private OrderType orderType;

    public Integer getPage() {
        return page;
    }

    public PageAndSizeAndType setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public PageAndSizeAndType setSize(Integer size) {
        this.size = size;
        return this;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public PageAndSizeAndType setOrderType(OrderType orderType) {
        this.orderType = orderType;
        return this;
    }
}
