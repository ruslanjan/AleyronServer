package com.aleyron.services.manager.data;

import com.aleyron.services.booker.data.InvoiceStatus;

public class IdAndStatus {
    private String id;
    private InvoiceStatus status;

    public String getId() {
        return id;
    }

    public IdAndStatus setId(String id) {
        this.id = id;
        return this;
    }

    public InvoiceStatus getStatus() {
        return status;
    }

    public IdAndStatus setStatus(InvoiceStatus status) {
        this.status = status;
        return this;
    }
}
