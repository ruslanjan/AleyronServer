package com.aleyron.services.manager.data;

import com.aleyron.services.order.documents.Order;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/23/18
 */
public class InvoiceIdAndOrder<T extends Order> {
    private String invoiceId;
    private T order;

    public String getInvoiceId() {
        return invoiceId;
    }

    public InvoiceIdAndOrder setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
        return this;
    }

    public T getOrder() {
        return order;
    }

    public InvoiceIdAndOrder setOrder(T order) {
        this.order = order;
        return this;
    }
}
