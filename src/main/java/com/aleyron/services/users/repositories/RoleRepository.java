package com.aleyron.services.users.repositories;

import com.aleyron.services.users.documents.Privilege;
import com.aleyron.services.users.documents.Role;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Map;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/19/18
 */
public interface RoleRepository extends MongoRepository < Role, String > {
	Role getByName(String name);

	Role getById(String id);

	boolean existsByPrivilegesContaining(Privilege privilege);

	@Query("?0")
	Page < Role > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

}
