package com.aleyron.services.users.repositories;

import com.aleyron.services.users.documents.Privilege;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Map;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/19/18
 */
public interface PrivilegeRepository extends MongoRepository < Privilege, String > {
	Privilege getByName(String name);

	Privilege getById(String id);

	Privilege getByPattern(String pattern);

	boolean existsByPattern(String pattern);

	@Query("?0")
	Page < Privilege > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

}
