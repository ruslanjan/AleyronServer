/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.users.repositories;

import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.services.users.documents.Privilege;
import com.aleyron.services.users.documents.Role;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Map;

public interface AleyronUserRepository extends MongoRepository < AleyronUser, String > {

	boolean existsByUsername(String username);

	AleyronUser getByUsername(String username);

	AleyronUser findByUsername(String username);

	AleyronUser getById(String id);

	boolean existsByRolesContaining(Role role);

	boolean existsByPrivilegesContaining(Privilege privilege);

	@Query("?0")
	Page < AleyronUser > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

	List < AleyronUser > findAllByRolesContaining(Role role);

	@Query("{ '?0': { $eq: '?1' } }")
	List < AleyronUser > getFiltered(String filterBy, String filter, Sort sort);
}