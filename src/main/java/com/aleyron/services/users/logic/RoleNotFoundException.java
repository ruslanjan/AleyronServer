package com.aleyron.services.users.logic;

public class RoleNotFoundException extends Throwable {
    private final String message;

    RoleNotFoundException(String roleName) {
        message = "no role with name: " + roleName;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
