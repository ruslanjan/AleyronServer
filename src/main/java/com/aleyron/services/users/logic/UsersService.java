/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.users.logic;

import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.services.users.documents.Privilege;
import com.aleyron.services.users.documents.Role;
import com.aleyron.services.users.repositories.AleyronUserRepository;
import com.aleyron.services.users.repositories.PrivilegeRepository;
import com.aleyron.services.users.repositories.RoleRepository;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {
	private final AleyronUserRepository aleyronUserRepository;
	private final RoleRepository roleRepository;
	private final PrivilegeRepository privilegeRepository;

	@Autowired
	public UsersService(AleyronUserRepository aleyronUserRepository, RoleRepository roleRepository, PrivilegeRepository privilegeRepository) {
		this.aleyronUserRepository = aleyronUserRepository;
		this.roleRepository = roleRepository;
		this.privilegeRepository = privilegeRepository;
	}

	public AleyronUser saveUser(AleyronUser aleyronUser) {
		if (aleyronUser.getId() != null) {
			AleyronUser user = aleyronUserRepository.getById(aleyronUser.getId());
			aleyronUser = copyUser(user, aleyronUser);
		}
		return aleyronUserRepository.save(aleyronUser);
	}

	private AleyronUser copyUser(AleyronUser to, AleyronUser from) {
		if (from.getId() != null) to.setId(from.getId());
		if (from.getFirstName() != null) to.setFirstName(from.getFirstName());
		if (from.getLastName() != null) to.setLastName(from.getLastName());
		if (from.getUsername() != null) to.setUsername(from.getUsername());
		if (from.getPassword() != null) to.copyPassword(from.getPassword());
		if (from.getPhone() != null) to.setPhone(from.getPhone());
		if (from.getEmail() != null) to.setEmail(from.getEmail());
		if (from.getRoles() != null) to.setRoles(from.getRoles());
		if (from.getPrivileges() != null) to.setPrivileges(from.getPrivileges());

		return to;
	}

	public Role writeRole(Role role) {
		return roleRepository.save(role);
	}

	public List < Role > getAllRoles( ) {
		return roleRepository.findAll();
	}

	public List < Privilege > getAllPrivileges( ) {
		return privilegeRepository.findAll();
	}

	public Role getRoleByName(String name) {
		return roleRepository.getByName(name);
	}

	public Privilege getPrivilegeByName(String name) {
		return privilegeRepository.getByName(name);
	}

	public AleyronUser deleteAleyronUserById(String id) {
		AleyronUser aleyronUser = aleyronUserRepository.getById(id);
		aleyronUserRepository.deleteById(id);
		return aleyronUser;
	}

	public Role deleteRoleById(String id) {
		Role role = roleRepository.getById(id);
		if (!aleyronUserRepository.existsByRolesContaining(role)) {
			roleRepository.deleteById(id);
		}
		return role;
	}

	public Privilege deletePrivilegeById(String id) {
		Privilege privilege = privilegeRepository.getById(id);
		if (!aleyronUserRepository.existsByPrivilegesContaining(privilege)
			&& !roleRepository.existsByPrivilegesContaining(privilege)) {
			privilegeRepository.deleteById(id);
		}
		return privilege;
	}

	public AleyronUser findByUsername(String username) {
		return aleyronUserRepository.findByUsername(username);
	}

	public Privilege writePrivilege(Privilege privilege) {
		return privilegeRepository.save(privilege);
	}

	public AleyronUser getById(String id) {
		return aleyronUserRepository.getById(id);
	}

	public Role getRoleById(String id) {
		return roleRepository.getById(id);
	}

	public Privilege getPrivilegeById(String id) {
		return privilegeRepository.getById(id);
	}

	public List < Privilege > readPrivilegesPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);
		return privilegeRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}

	public List < Role > readRolesPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);
		return roleRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}

	public List < AleyronUser > readAleyronUsersPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);
		return aleyronUserRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}

	public List < AleyronUser > readAleyronUsers(String sortBy, Sort.Direction sortDirection, String filterBy, String filter) {
		Sort sort = Sort.by(Sort.Direction.ASC, "id");

		if (sortBy != null && sortDirection != null) {
			sort = Sort.by(sortDirection, sortBy);
		}

		if (filter != null && filterBy != null) {
			return aleyronUserRepository.getFiltered(filterBy, filter, sort);
		} else {
			return aleyronUserRepository.findAll(sort);
		}
	}

	public List < AleyronUser > getAllWithRole(String roleName) throws RoleNotFoundException {
		Role role = roleRepository.getByName(roleName);
		if (role == null) {
			throw new RoleNotFoundException(roleName);
		}

		return aleyronUserRepository.findAllByRolesContaining(role);
	}
}
