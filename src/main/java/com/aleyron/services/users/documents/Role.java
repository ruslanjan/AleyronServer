package com.aleyron.services.users.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/17/18
 */
@Document
public class Role {
    @Id
    private String id;
    @Indexed(sparse = true, unique = true)
    private String name;

    private String description;

    @DBRef
    @Indexed(sparse = true)
    private List<Privilege> privileges;

    public String getId() {
        return id;
    }

    public Role setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Role setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public Role setPrivileges(List<Privilege> privileges) {
        this.privileges = privileges;
        return this;
    }
}
