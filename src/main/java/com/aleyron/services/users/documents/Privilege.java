package com.aleyron.services.users.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/19/18
 */
@Document
public class Privilege {

    @Id
    private String id;

    private String name;

    @Indexed(sparse = true, unique = true)
    private String pattern;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Privilege setName(String name) {
        this.name = name;
        return this;
    }

    public String getPattern() {
        return pattern;
    }

    public Privilege setPattern(String pattern) {
        this.pattern = pattern;
        return this;
    }
}
