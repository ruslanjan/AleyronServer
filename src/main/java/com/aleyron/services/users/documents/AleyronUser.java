/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.users.documents;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Document
public class AleyronUser {

    public final static PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    private @Id
    String id;

    private String firstName;
    private String lastName;

    @Indexed(unique = true)
    private String username;


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @DBRef
    @Indexed(sparse = true)
    private List<Role> roles;

    @DBRef
    @Indexed(sparse = true)
    private List<Privilege> privileges;

    private String email;
    private String phone;

    public AleyronUser() {
    }

    public AleyronUser(String username, String password, List<Role> roles) {
        this.username = username;
        setPassword(password);
        this.roles = roles;
    }

    public AleyronUser(String firstName, String lastName, String username, String password, List<Role> roles, String email, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        setPassword(password);
        this.roles = roles;
        this.email = email;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public AleyronUser setId(String id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public AleyronUser setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public AleyronUser setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public AleyronUser setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public AleyronUser setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
        return this;
    }

    public AleyronUser copyPassword(String password) {
        this.password = (password);
        return this;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public AleyronUser setRoles(List<Role> roles) {
        this.roles = roles;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public AleyronUser setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public AleyronUser setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public AleyronUser setPrivileges(List<Privilege> privileges) {
        this.privileges = privileges;
        return this;
    }

}

