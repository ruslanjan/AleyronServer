package com.aleyron.services.users.controllers;

import com.aleyron.services.users.documents.Role;
import com.aleyron.services.users.logic.UsersService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/20/18
 */
@RestController
@RequestMapping("/api/users/roles")
public class RolesController {

	private UsersService usersService;

	/**
	 * Коструктор
	 *
	 * @param usersService users Service layer
	 */
	@Autowired
	public RolesController(UsersService usersService) {
		this.usersService = usersService;
	}

	@PostMapping("/create")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.roles.createRole')")
	public Role createRole(@RequestBody Role role) {
		return usersService.writeRole(role);
	}

	@GetMapping("/getByName")
	public Role getRoleByName(String name) {
		return usersService.getRoleByName(name);
	}

	@GetMapping("/getById")
	public Role getRoleById(String id) {
		return usersService.getRoleById(id);
	}

	@GetMapping("/getAll")
	public List < Role > getAllRoles( ) {
		return usersService.getAllRoles();
	}

	@GetMapping("/getPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.roles.get')")
	public List < Role > getPage(Integer page,
	                             Integer size,
	                             @RequestParam(value = "sorts[]", required = false)
		                             String[] sorts,
	                             @RequestParam(value = "filters[]", required = false)
		                             String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
		return usersService.readRolesPage(page, size, _filters, _sorts);
	}

	@PostMapping("/deleteById")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.roles.deleteRoleById')")
	public Role deleteRoleById(@RequestBody String id) {
		return usersService.deleteRoleById(id);
	}
}
