/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.users.controllers;

import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.services.users.logic.RoleNotFoundException;
import com.aleyron.services.users.logic.UsersService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Spring Component
 * RestController Авторизации и регистрированя.
 */
@RestController
@RequestMapping("/api/users")
public class UsersController {
	private UsersService usersService;

	/**
	 * Коструктор
	 *
	 * @param usersService users Service layer
	 */
	@Autowired
	public UsersController(UsersService usersService) {
		this.usersService = usersService;
	}

	/**
	 * Регистрация пользователей
	 *
	 * @return Созданного пользователя
	 */
	@PostMapping("/register")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.register')" +
		"and #aleyronUser.username != null  " +
		"and #aleyronUser.password != null ")
	public AleyronUser register(@RequestBody AleyronUser aleyronUser,
	                            HttpServletResponse response) {
		if (aleyronUser.getUsername().compareTo("") == 0
			|| aleyronUser.getPassword().compareTo("") == 0
			|| aleyronUser.getUsername().contains(" ")
			|| aleyronUser.getUsername().contains("\n")
			|| aleyronUser.getPassword().contains(" ")
			|| aleyronUser.getPassword().contains("\n")) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			return null;
		}
		return usersService.saveUser(aleyronUser);
	}

	@PostMapping("/update")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.update')")
	public AleyronUser update(@RequestBody AleyronUser aleyronUser,
	                          HttpServletResponse response) {
		if (aleyronUser.getUsername().compareTo("") == 0
			|| aleyronUser.getUsername().contains(" ")
			|| aleyronUser.getUsername().contains("\n")) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			return null;
		}

		return usersService.saveUser(aleyronUser);
	}

	@PostMapping("/deleteById")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.delete')")
	public AleyronUser deleteUserById(@RequestBody String id, HttpServletResponse response) {
		return usersService.deleteAleyronUserById(id);
	}

	@GetMapping("/getPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.get')")
	public List < AleyronUser > getPage(Integer page,
	                                    Integer size,
	                                    @RequestParam(value = "sorts[]", required = false)
		                                    String[] sorts,
	                                    @RequestParam(value = "filters[]", required = false)
		                                    String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
		return usersService.readAleyronUsersPage(page, size, _filters, _sorts);
	}

	@GetMapping("/getAll")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.get')")
	public List < AleyronUser > getAll(String sortBy,
	                                   Sort.Direction sortDirection,
	                                   String filterBy,
	                                   String filter) {
		return usersService.readAleyronUsers(sortBy, sortDirection, filterBy, filter);
	}

	@GetMapping("/getAllWithRole")
	@PreAuthorize("@securityService.hasPattern(principal, 'users.get')")
	public List < AleyronUser > getAll(String roleName, HttpServletResponse response) throws IOException {
		try {
			return usersService.getAllWithRole(roleName);
		} catch (RoleNotFoundException e) {
			response.sendError(404, e.getMessage());
			return Collections.emptyList();
		}
	}

	@GetMapping("/getByUsername")
	public AleyronUser findByUsername(String username) {
		return usersService.findByUsername(username);
	}

	@GetMapping("/getById")
	public AleyronUser getById(String id) {
		return usersService.getById(id);
	}
}