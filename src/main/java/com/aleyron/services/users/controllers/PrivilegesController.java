package com.aleyron.services.users.controllers;

import com.aleyron.services.users.documents.Privilege;
import com.aleyron.services.users.logic.UsersService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/20/18
 */
@RestController
@RequestMapping("/api/users/privileges")
public class PrivilegesController {

    private UsersService usersService;

    /**
     * Коструктор
     *
     * @param usersService users Service layer
     */
    @Autowired
    public PrivilegesController(UsersService usersService) {
        this.usersService = usersService;
    }

    @PostMapping("/create")
    @PreAuthorize("@securityService.hasPattern(principal, 'users.privileges.createPrivilege')")
    public Privilege createPrivilege(@RequestBody Privilege privilege) {
        return usersService.writePrivilege(privilege);
    }

    @GetMapping("/getByName")
    public Privilege getPrivilegeByName(String name) {
        return usersService.getPrivilegeByName(name);
    }

    @GetMapping("/getById")
    public Privilege getPrivilegeById(String id) {
        return usersService.getPrivilegeById(id);
    }

    @GetMapping("/getAll")
    public List<Privilege> getAllPrivileges(String name) {
        return usersService.getAllPrivileges();
    }


    @GetMapping("/getPage")
    @PreAuthorize("@securityService.hasPattern(principal, 'users.privileges.getPage')")
    public List<Privilege> getPage(Integer page,
	                             Integer size,
	                             @RequestParam(value = "sorts[]", required = false)
		                                 String[] sorts,
	                             @RequestParam(value = "filters[]", required = false)
		                                 String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
        return usersService.readPrivilegesPage(page, size, _filters, _sorts);
    }

    @PostMapping("/deleteById")
    @PreAuthorize("@securityService.hasPattern(principal, 'users.privileges.deletePrivilegeById')")
    public Privilege deletePrivilegeById(@RequestBody String id) {
        return usersService.deletePrivilegeById(id);
    }
}
