package com.aleyron.services.chat.repositories;

import com.aleyron.services.chat.documents.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MessageRepository extends MongoRepository<Message, String> {
    Message getById(String id);
}
