package com.aleyron.services.chat.repositories;

import com.aleyron.services.chat.documents.Room;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoomRepository extends MongoRepository<Room, String> {
    Room getById(String id);

    Room getByName(String name);

    void deleteByName(String name);
}
