package com.aleyron.services.chat.repositories;

import com.aleyron.services.chat.documents.ChatClient;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChatClientRepository extends MongoRepository<ChatClient, String> {
    ChatClient getById(String id);

    ChatClient getByAleyronUser_Username(String name);

    void deleteByAleyronUser_Username(String name);
}
