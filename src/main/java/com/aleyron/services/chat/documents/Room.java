package com.aleyron.services.chat.documents;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
public class Room {

    @Id
    private String id;
    private String name;
    @DBRef
    private List<ChatClient> clients;

    Room() {
        clients = new ArrayList<>();
        name = "";
    }

    public String getId() {
        return id;
    }

    public Room setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Room setName(String name) {
        this.name = name;
        return this;
    }

    public List<ChatClient> getClients() {
        return clients;
    }


    public Room setClients(List<ChatClient> clients) {
        this.clients = clients;
        return this;
    }
}
