package com.aleyron.services.chat.documents;

import com.aleyron.services.users.documents.AleyronUser;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class ChatClient {
    @Id
    private String id;
    @DBRef
    private AleyronUser aleyronUser;
    @DBRef
    private List<Room> rooms;

    public String getId() {
        return id;
    }

    public ChatClient setId(String id) {
        this.id = id;
        return this;
    }

    public AleyronUser getAleyronUser() {
        return aleyronUser;
    }

    public ChatClient setAleyronUser(AleyronUser aleyronUser) {
        this.aleyronUser = aleyronUser;
        return this;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public ChatClient setRooms(List<Room> rooms) {
        this.rooms = rooms;
        return this;
    }
}
