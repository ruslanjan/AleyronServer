package com.aleyron.services.chat.logic;

import com.aleyron.services.chat.documents.Room;
import com.aleyron.services.chat.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomManagerService {

    private RoomRepository roomRepository;

    @Autowired
    RoomManagerService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;

    }

    public Room getRoom(String id) {
        return roomRepository.getById(id);
    }

    public Room getRoomByName(String name) {
        return roomRepository.getByName(name);
    }

    public Room saveRoom(Room room) {
        return roomRepository.save(room);
    }

    public void deleteRoom(String id) {
        roomRepository.deleteById(id);
    }

    public void deleteRoomByName(String id) {
        roomRepository.deleteByName(id);
    }
}
