package com.aleyron.services.chat.logic;

import com.aleyron.services.chat.documents.ChatClient;
import com.aleyron.services.chat.repositories.ChatClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@SuppressWarnings({"UnusedReturnValue", "WeakerAccess"})
@Service
public class ClientManagerService {

    private ChatClientRepository chatClientRepository;

    @Autowired
    ClientManagerService(ChatClientRepository chatClientRepository) {
        this.chatClientRepository = chatClientRepository;
    }

    public ChatClient getClient(String id) {
        return chatClientRepository.getById(id);
    }

    public ChatClient getClientByName(String name) {
        return chatClientRepository.getByAleyronUser_Username(name);
    }

    public ChatClient saveClient(ChatClient client) {
        return chatClientRepository.save(client);
    }

    public void deleteClient(String id) {
        chatClientRepository.deleteById(id);
    }

    public void deleteClientByName(String name) {
        chatClientRepository.deleteByAleyronUser_Username(name);
    }
}
