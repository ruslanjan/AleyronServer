package com.aleyron.services.chat.logic;

import com.aleyron.services.chat.documents.Message;
import com.aleyron.services.chat.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageManagerService {

    private MessageRepository messageRepository;

    @Autowired
    MessageManagerService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Message getMessage(String id) {
        return messageRepository.getById(id);
    }

    public Message saveMessage(Message message) {
        return messageRepository.save(message);
    }
}
