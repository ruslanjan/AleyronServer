package com.aleyron.services.chat.logic;

import com.aleyron.services.chat.documents.ChatClient;
import com.aleyron.services.chat.documents.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatService {

    private ClientManagerService clientManagerService;
    private RoomManagerService roomManagerService;

    @Autowired
    ChatService(ClientManagerService clientManagerService, RoomManagerService roomManagerService) {
        this.clientManagerService = clientManagerService;
        this.roomManagerService = roomManagerService;
    }

    public void connect(ChatClient client, Room room) {
        List<Room> rooms = client.getRooms();
        List<ChatClient> clients = room.getClients();

        rooms.add(room);
        clients.add(client);

        client.setRooms(rooms);
        room.setClients(clients);

        clientManagerService.saveClient(client);
        roomManagerService.saveRoom(room);
    }

    public void getUpdates(String roomId, Long timestamp) {

    }
}
