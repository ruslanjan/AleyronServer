/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.utils.services.plate_placement.repositories;

import com.aleyron.services.utils.services.plate_placement.documents.PlatePlacementTask;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Map;

public interface PlatePlacementTaskRepository extends MongoRepository < PlatePlacementTask, String > {
	PlatePlacementTask findByName(String name);

	PlatePlacementTask getById(String id);

	@Query("?0")
	Page < PlatePlacementTask > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

}
