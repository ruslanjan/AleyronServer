/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.utils.services.plate_placement.logic;

import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.services.users.repositories.AleyronUserRepository;
import com.aleyron.services.utils.services.plate_placement.documents.PlatePlacementTask;
import com.aleyron.services.utils.services.plate_placement.repositories.PlatePlacementTaskRepository;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class DefaultPlacePlacementService implements PlacePlacementService {

	private PlatePlacementTaskRepository taskRepository;
	private AleyronUserRepository aleyronUserRepository;

	@Autowired
	public DefaultPlacePlacementService(PlatePlacementTaskRepository taskRepository, AleyronUserRepository aleyronUserRepository) {
		this.taskRepository = taskRepository;
		this.aleyronUserRepository = aleyronUserRepository;
	}

	@Override
	public void createTask(PlatePlacementTask platePlacementTask) {
		taskRepository.save(platePlacementTask);
	}

	@Override
	public AleyronUser userFromPrincipal(Principal principal) {
		return aleyronUserRepository.getByUsername(
			principal.getName()
		);
	}

	@Override
	public void deleteTask(String id, AleyronUser user) {

		if (id == null || !taskRepository.existsById(id)) {
			throw new IllegalArgumentException("illegal id");
		}

		PlatePlacementTask platePlacementTask = taskRepository.getById(id);
		taskRepository.deleteById(id);
	}

	@Override
	public PlatePlacementTask getTask(String id) {
		try {
			return taskRepository.findById(id).orElse(null);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public PlatePlacementTask getTaskByName(String name) {
		return taskRepository.findByName(name);
	}

	@Override
	public List < PlatePlacementTask > getTasks( ) {
		return taskRepository.findAll();
	}

	@Override
	public List < PlatePlacementTask > readPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);
		return taskRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}
}
