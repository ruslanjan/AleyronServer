/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.utils.services.plate_placement.controllers;

import com.aleyron.services.utils.services.plate_placement.documents.PlatePlacementTask;
import com.aleyron.services.utils.services.plate_placement.logic.PlacePlacementService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/api/utils/plate_placement/tasks")
public class PlatePlacementController {

	private PlacePlacementService placePlacementService;

	@Autowired
	PlatePlacementController(PlacePlacementService placePlacementService) {
		this.placePlacementService = placePlacementService;
	}

	@PostMapping("/create")
	public void createTask(@RequestBody PlatePlacementTask platePlacementTask,
	                       HttpServletRequest request, HttpServletResponse response) {
		placePlacementService.createTask(
			platePlacementTask.setUser(
				request.getUserPrincipal().getName()
			)
		);
	}

	@PostMapping("/deleteById")
	public void deleteTaskById(String id,
	                           HttpServletRequest request) {
		placePlacementService.deleteTask(
			id,
			placePlacementService.userFromPrincipal(request.getUserPrincipal())
		);
	}

	@GetMapping("/getById")
	public PlatePlacementTask getTask(String id) {
		return placePlacementService.getTask(id);
	}

	@GetMapping("/getByName")
	public PlatePlacementTask getTaskByName(String name) {
		return placePlacementService.getTaskByName(name);
	}

	@GetMapping("/getAll")
	public List < PlatePlacementTask > getTasks( ) {
		return placePlacementService.getTasks();
	}

	@GetMapping("/getPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.history.get')")
	public List < PlatePlacementTask > getPage(Integer page,
	                                           Integer size,
	                                           @RequestParam(value = "sorts[]", required = false)
		                                           String[] sorts,
	                                           @RequestParam(value = "filters[]", required = false)
		                                           String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
		return placePlacementService.readPage(page, size, _filters, _sorts);
	}


}
