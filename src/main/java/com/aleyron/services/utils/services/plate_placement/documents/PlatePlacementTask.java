/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.utils.services.plate_placement.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;

@Document
public class PlatePlacementTask {

    @Id
    private String id;

    private String name;
    @Indexed(sparse = true)
    private String user;
    private String type;
    private int plateNumber;
    private int capacity;
    private Products[] products;

    public String getId() {
        return id;
    }

    public PlatePlacementTask setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public PlatePlacementTask setName(String name) {
        this.name = name;
        return this;
    }

    public String getUser() {
        return user;
    }

    public PlatePlacementTask setUser(String user) {
        this.user = user;
        return this;
    }

    public String getType() {
        return type;
    }

    public PlatePlacementTask setType(String type) {
        this.type = type;
        return this;
    }

    public int getPlateNumber() {
        return plateNumber;
    }

    public PlatePlacementTask setPlateNumber(int plateNumber) {
        this.plateNumber = plateNumber;
        return this;
    }

    public int getCapacity() {
        return capacity;
    }

    public PlatePlacementTask setCapacity(int capacity) {
        this.capacity = capacity;
        return this;
    }

    public Products[] getProducts() {
        return products;
    }

    public PlatePlacementTask setProducts(Products[] products) {
        this.products = products;
        return this;
    }

    @Override

    public String toString() {
        return "PlatePlacementTask{" +
                "type='" + type + '\'' +
                ", plateNumber=" + plateNumber +
                ", capacity=" + capacity +
                ", products=" + (products == null ? null : Arrays.asList(products)) +
                '}';
    }
}
