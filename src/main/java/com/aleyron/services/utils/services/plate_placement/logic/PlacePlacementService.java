/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.utils.services.plate_placement.logic;

import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.services.utils.services.plate_placement.documents.PlatePlacementTask;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import org.springframework.data.domain.Sort;

import java.security.Principal;
import java.util.List;

public interface PlacePlacementService {
	void createTask(PlatePlacementTask platePlacementTask);

	AleyronUser userFromPrincipal(Principal principal);

	void deleteTask(String id, AleyronUser user);

	PlatePlacementTask getTask(String id);

	PlatePlacementTask getTaskByName(String name);

	List < PlatePlacementTask > getTasks( );

	List < PlatePlacementTask > readPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts);
}
