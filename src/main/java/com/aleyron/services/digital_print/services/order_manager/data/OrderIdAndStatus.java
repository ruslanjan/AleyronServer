package com.aleyron.services.digital_print.services.order_manager.data;

import com.aleyron.services.order.data.PrintOrderStatus;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/25/18
 */
public class OrderIdAndStatus {
    private String orderId;
    private PrintOrderStatus orderStatus;

    public String getOrderId() {
        return orderId;
    }

    public OrderIdAndStatus setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public PrintOrderStatus getOrderStatus() {
        return orderStatus;
    }

    public OrderIdAndStatus setOrderStatus(PrintOrderStatus orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }
}
