package com.aleyron.services.digital_print.services.storage.documents;

import com.aleyron.services.digital_print.services.storage.data.OperationCause;
import com.aleyron.services.digital_print.services.storage.data.OperationExchange;
import com.aleyron.services.digital_print.services.storage.data.OperationType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Deprecated
@Document
public class StorageOperation {
    @Id
    private String id;

    private OperationType type;

    private String orderId;
    private String user;
    private Date date = new Date();
    private OperationExchange exchange;
    private OperationCause cause;

    public String getUser() {
        return user;
    }

    public StorageOperation setUser(String user) {
        this.user = user;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public StorageOperation setDate(Date date) {
        this.date = date;
        return this;
    }

    public OperationCause getCause() {
        return cause;
    }

    public StorageOperation setCause(OperationCause cause) {
        this.cause = cause;
        return this;
    }

    public String getId() {
        return id;
    }

    public StorageOperation setId(String id) {
        this.id = id;
        return this;
    }

    public OperationType getType() {
        return type;
    }

    public StorageOperation setType(OperationType type) {
        this.type = type;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public StorageOperation setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public OperationExchange getExchange() {
        return exchange;
    }

    public StorageOperation setExchange(OperationExchange exchange) {
        this.exchange = exchange;
        return this;
    }
}
