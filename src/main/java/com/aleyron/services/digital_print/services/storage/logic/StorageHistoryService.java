package com.aleyron.services.digital_print.services.storage.logic;

import com.aleyron.services.digital_print.services.storage.documents.StorageOperation;
import com.aleyron.services.digital_print.services.storage.repositories.StorageOperationRepository;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Deprecated
@Service
public class StorageHistoryService {
	private StorageOperationRepository historyRepository;

	@Autowired
	public StorageHistoryService(StorageOperationRepository historyRepository) {
		this.historyRepository = historyRepository;
	}

	public StorageOperation getOperation(String id) {
		return historyRepository.getById(id);
	}

	public List < StorageOperation > getAll( ) {
		return historyRepository.findAll();
	}


	public List < StorageOperation > readPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);
		return historyRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}
}
