/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.digital_print.services.storage.controllers;

import com.aleyron.services.digital_print.services.storage.documents.StorageItem;
import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/digital_print/storage/items")
public class StorageItemController {

	private final StorageItemService service;

	@Autowired
	StorageItemController(StorageItemService StorageItemService) {
		this.service = StorageItemService;
	}

	@Deprecated
	@PostMapping("/create")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.create')")
	public StorageItem create(@RequestBody StorageItem item) {
		return service.saveItem(item);
	}

	@PostMapping("/delete")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.delete')")
	public void delete(@RequestBody String id, HttpServletRequest request) {
		service.deleteItem(id, request.getUserPrincipal().getName());
	}

	@GetMapping("/getById")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.get')")
	public StorageItem getItem(String id) {
		return service.readItem(id);
	}

	@GetMapping("/getAll")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.get')")
	public List < StorageItem > getAll( ) {
		return service.readAll();
	}

	@GetMapping("/getPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.get')")
	public List < StorageItem > getPage(Integer page,
	                             Integer size,
	                             @RequestParam(value = "sorts[]", required = false)
		                                 String[] sorts,
	                             @RequestParam(value = "filters[]", required = false)
		                                 String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
		return service.readPage(page, size, _filters, _sorts);
	}

	@GetMapping("/getTextSearchResultsPage")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.get')")
	public List < StorageItem > getTextSearchResultsPage(Integer page,
	                                                     Integer size,
	                                                     String filter) {
		return service.readTextSearchResultsPage(page, size, filter);
	}

	@GetMapping("/getAllWithOrderType")
	@PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.items.get')")
	public List < StorageItem > getAllWithOrderType(OrderType orderType) {
		return service.readAllWithOrderType(orderType);
	}

}
