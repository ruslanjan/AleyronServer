package com.aleyron.services.digital_print.services.storage.repositories;

import com.aleyron.services.digital_print.services.storage.documents.StorageGroup;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Map;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/3/18
 */
@Deprecated
public interface StorageGroupRepository extends MongoRepository < StorageGroup, String > {
	StorageGroup getByName(String name);

	StorageGroup getById(String id);

	@Query("?0")
	Page < StorageGroup > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

}
