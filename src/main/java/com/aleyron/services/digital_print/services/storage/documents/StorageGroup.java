package com.aleyron.services.digital_print.services.storage.documents;

import com.aleyron.services.digital_print.services.storage.data.StorageGroupType;
import com.aleyron.services.digital_print.services.storage.exceptions.InvalidUnitsException;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/3/18
 */
@Deprecated
@Document
public class StorageGroup {
    @Id
    private String id;

    @Indexed(unique = true, sparse = true)
    @TextIndexed
    private String name;
    private StorageGroupType type = StorageGroupType.BASIC_GROUP;
    @DBRef(lazy = true)
    private List<StorageItem> storageItemList;

    public List<StorageItem> getStorageItemList() {
        return storageItemList;
    }

    public StorageGroup setStorageItemList(List<StorageItem> storageItemList) throws InvalidUnitsException {
        this.storageItemList = storageItemList;
        return this;
    }

    public String getId() {
        return id;
    }

    public StorageGroup setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public StorageGroup setName(String name) {
        this.name = name;
        return this;
    }

    public StorageGroupType getType() {
        return type;
    }

    public StorageGroup setType(StorageGroupType type) {
        this.type = type;
        return this;
    }
}
