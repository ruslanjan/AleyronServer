package com.aleyron.services.digital_print.services.storage.documents;

import com.aleyron.services.digital_print.services.storage.data.MetricUnitEnum;
import com.aleyron.services.digital_print.services.storage.data.OperationExchange;
import com.aleyron.services.order.data.OrderType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * @author Karl Meinkopf
 */
@Document
public class StorageItem {
    @Id
    private String id;

    @DBRef(lazy = true)
    @Deprecated
    private StorageGroup group;

    @Indexed(unique = true, sparse = true)
    @TextIndexed
    private String name;

    private double quantity;
    private double cost;
    private double height;
    private double width;
    private MetricUnitEnum unit;
    private List< OrderType > allowedOrders;

    public String getId() {
        return id;
    }

    public StorageItem setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public StorageItem setName(String name) {
        this.name = name;
        return this;
    }

    public double getQuantity() {
        return quantity;
    }

    public StorageItem setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    public MetricUnitEnum getUnit() {
        return unit;
    }

    public StorageItem setUnit(MetricUnitEnum unit) {
        this.unit = unit;
        return this;
    }

    public double getCost() {
        return cost;
    }

    public StorageItem setCost(double cost) {
        this.cost = cost;
        return this;
    }

    @Deprecated
    public OperationExchange toOperationExchange() {
        return new OperationExchange()
                .setCost(cost)
                .setName(name)
                .setQuantity(quantity)
                .setUnit(unit);
    }

    @Deprecated
    public StorageGroup getGroup() {
        return group;
    }

    public StorageItem setGroup(StorageGroup group) {
        this.group = group;
        return this;
    }

    public double getHeight() {
        return height;
    }

    public StorageItem setHeight(double height) {
        this.height = height;
        return this;
    }

    public double getWidth() {
        return width;
    }

    public StorageItem setWidth(double width) {
        this.width = width;
        return this;
    }

    public List < OrderType > getAllowedOrders( ) {
        return allowedOrders;
    }

    public StorageItem setAllowedOrders(List < OrderType > allowedOrders) {
        this.allowedOrders = allowedOrders;
        return this;
    }
}
