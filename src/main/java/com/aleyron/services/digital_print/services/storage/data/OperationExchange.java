package com.aleyron.services.digital_print.services.storage.data;

import com.aleyron.services.digital_print.services.storage.documents.StorageItem;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Karl Meinkopf
 */

@Deprecated
@Document
public class OperationExchange {
    private String name;

    private double quantity;
    private MetricUnitEnum unit;

    private double cost;

    public String getName() {
        return name;
    }

    public OperationExchange setName(String name) {
        this.name = name;
        return this;
    }

    public double getQuantity() {
        return quantity;
    }

    public OperationExchange setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    public MetricUnitEnum getUnit() {
        return unit;
    }

    public OperationExchange setUnit(MetricUnitEnum unit) {
        this.unit = unit;
        return this;
    }

    public double getCost() {
        return cost;
    }

    public OperationExchange setCost(double cost) {
        this.cost = cost;
        return this;
    }

    public StorageItem toStorageItem() {
        return (new StorageItem())
                .setCost(cost)
                .setUnit(unit)
                .setName(name)
                .setQuantity(quantity);
    }


}
