package com.aleyron.services.digital_print.services.storage.controllers;

import com.aleyron.services.digital_print.services.storage.documents.StorageOperation;
import com.aleyron.services.digital_print.services.storage.exceptions.InvalidUnitsException;
import com.aleyron.services.digital_print.services.storage.exceptions.OutOfStockException;
import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Deprecated
//@RestController
@RequestMapping("/api/digital_print/storage/operations")
public class StorageOperationController {
    private final StorageItemService service;

    @Autowired
    StorageOperationController(StorageItemService StorageItemService) {
        this.service = StorageItemService;
    }

    @PostMapping("/perform")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.operations.perform')")
    public void performOperation(@RequestBody StorageOperation operation, HttpServletRequest request, HttpServletResponse response) throws IOException {
        operation.setUser(request.getUserPrincipal().getName());
        try {
            service.performOperation(
                    operation);
        } catch (InvalidUnitsException e) {
            e.printStackTrace();
        } catch (OutOfStockException e) {
            response.sendError(423, "Out of stock");
        }
    }

}
