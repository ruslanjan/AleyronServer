package com.aleyron.services.digital_print.services.storage.controllers;

import com.aleyron.services.digital_print.services.storage.documents.StorageGroup;
import com.aleyron.services.digital_print.services.storage.logic.StorageGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/3/18
 */
@Deprecated
@Component
//@RestController
@RequestMapping("/api/digital_print/storage/groups")
public class StorageGroupController {
    private StorageGroupService service;

    @Autowired
    public StorageGroupController(StorageGroupService service) {
        this.service = service;
    }

    @PostMapping("/create")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.groups.create')")
    public StorageGroup create(@RequestBody StorageGroup group) {
        return service.create(group);
    }

    @PostMapping("/delete")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.groups.delete')")
    public void delete(@RequestBody String id, HttpServletRequest request) {
        service.delete(id);
    }


    @GetMapping("/getById")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.groups.get')")
    public StorageGroup getGroup(String id) {
        return service.read(id);
    }

    @GetMapping("/getAll")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.groups.get')")
    public List<StorageGroup> findAll() {
        return service.readAll();
    }
}
