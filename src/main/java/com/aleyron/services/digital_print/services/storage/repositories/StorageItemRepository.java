package com.aleyron.services.digital_print.services.storage.repositories;

import com.aleyron.services.digital_print.services.storage.documents.StorageItem;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Map;

public interface StorageItemRepository extends MongoRepository < StorageItem, String > {
	StorageItem getById(String id);

	StorageItem getByName(String name);

	@Query("{ $text: { $search: '?0' } }")
	Page < StorageItem > getTextSearchResultsPage(String filter, Pageable pageable);

	@Query("?0")
	Page < StorageItem > getFilteredPage(Map < String, FilterOperation > filters, Pageable pageable);

	List < StorageItem > getAllByAllowedOrdersContains(OrderType orderType);
}