/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron.services.digital_print.services.storage.logic;

import com.aleyron.services.digital_print.services.storage.data.OperationExchange;
import com.aleyron.services.digital_print.services.storage.data.OperationType;
import com.aleyron.services.digital_print.services.storage.documents.StorageItem;
import com.aleyron.services.digital_print.services.storage.documents.StorageOperation;
import com.aleyron.services.digital_print.services.storage.exceptions.InvalidUnitsException;
import com.aleyron.services.digital_print.services.storage.exceptions.OutOfStockException;
import com.aleyron.services.digital_print.services.storage.repositories.StorageGroupRepository;
import com.aleyron.services.digital_print.services.storage.repositories.StorageItemRepository;
import com.aleyron.services.digital_print.services.storage.repositories.StorageOperationRepository;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@SuppressWarnings({ "WeakerAccess", "unused", "FieldCanBeLocal" })
@Service
public class StorageItemService {
	private final StorageOperationRepository operationRepository;
	private final StorageItemRepository itemRepository;
	private final StorageGroupRepository groupRepository;

	@Autowired
	StorageItemService(StorageItemRepository itemRepository, StorageOperationRepository operationRepository, StorageGroupRepository groupRepository) {
		this.itemRepository = itemRepository;
		this.operationRepository = operationRepository;
		this.groupRepository = groupRepository;
	}


	public StorageItem readItem(String itemId) {
		return itemRepository.getById(itemId);
	}

	public List < StorageItem > readAll( ) {
		return itemRepository.findAll();
	}

	public List < StorageItem > readPage(Integer page, Integer size, List < FilterRequest > filters, List < Sort > sorts) {
		FilteredPageRequest pr = PageRequestGenerator.getPageFilteredSorted(page, size, sorts, filters);

		return itemRepository.getFilteredPage(pr.getFilters(), pr.getPageable()).getContent();
	}

	public StorageItem saveItem(StorageItem item) {
		return itemRepository.save(item);
	}

	public void deleteItem(String itemId, String username) {
		StorageItem item = itemRepository.getById(itemId);
//		writeLog(new StorageOperation()
//			.setUser(username)
//			.setExchange(item.toOperationExchange())
//			.setCause(OperationCause.EDIT)
//			.setType(OperationType.OUTCOME));
		itemRepository.deleteById(itemId);
	}
	// endregion

	// region Operations

	@Deprecated
	public void performOperation(StorageOperation operation) throws InvalidUnitsException, OutOfStockException {
		if (operation.getType() == OperationType.INCOME) {
			income(operation);
		} else {
			outcome(operation);
		}
	}

	@Deprecated
	private void writeLog(StorageOperation operation) {
		operation.setDate(new Date());
//		operationRepository.save(operation);
	}

	@Deprecated
	private void income(StorageOperation operation) throws InvalidUnitsException {
		OperationExchange exchange = operation.getExchange();
		StorageItem storageItem = itemRepository.getByName(exchange.getName());

		if (storageItem == null) {
			writeLog(operation);
			saveItem(exchange.toStorageItem());
			return;
		}

		if (!Objects.equals(storageItem.getUnit(), exchange.getUnit()))
			throw new InvalidUnitsException();

		storageItem = storageItem.setQuantity(
			storageItem.getQuantity() + exchange.getQuantity()
		).setCost(
			storageItem.getCost() + exchange.getCost()
		);

		writeLog(operation);
		saveItem(storageItem);
	}

	@Deprecated
	private void outcome(StorageOperation operation) throws InvalidUnitsException, OutOfStockException {
		OperationExchange exchange = operation.getExchange();
		StorageItem storageItem = itemRepository.getByName(exchange.getName());

		if (storageItem == null) {
			throw new OutOfStockException();
		}

		if (!Objects.equals(storageItem.getUnit(), exchange.getUnit()))
			throw new InvalidUnitsException();

		if (storageItem.getQuantity() < exchange.getQuantity())
			throw new OutOfStockException();

		exchange.setCost(exchange.getQuantity() * storageItem.getCost() / storageItem.getQuantity());

		storageItem = storageItem.setQuantity(
			storageItem.getQuantity() - exchange.getQuantity()
		).setCost(
			storageItem.getCost() - exchange.getCost()
		);

		writeLog(operation.setExchange(exchange));
		saveItem(storageItem);
	}

	public List < StorageItem > readTextSearchResultsPage(Integer page, Integer size, String filter) {
		return itemRepository.getTextSearchResultsPage(filter, PageRequest.of(page, size)).getContent();
	}

	public List < StorageItem > readAllWithOrderType(OrderType filter) {
		return itemRepository.getAllByAllowedOrdersContains(filter);
	}
}
