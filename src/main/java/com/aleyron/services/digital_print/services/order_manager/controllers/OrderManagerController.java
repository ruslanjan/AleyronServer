package com.aleyron.services.digital_print.services.order_manager.controllers;

import com.aleyron.services.digital_print.services.order_manager.data.OrderIdAndStatus;
import com.aleyron.services.digital_print.services.order_manager.logic.OrderManagerService;
import com.aleyron.services.order.documents.DesignOrder;
import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.documents.Order;
import com.aleyron.services.order.logic.OrderService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/23/18
 */
@RestController
@RequestMapping("/api/digital_print/order_manager")
public class OrderManagerController {
    private OrderManagerService orderManagerService;
    private OrderService orderService;

    @Autowired
    public OrderManagerController(OrderManagerService orderManagerService, OrderService orderService) {
        this.orderManagerService = orderManagerService;
        this.orderService = orderService;
    }

    @GetMapping("/getDigitalPrintOrderById")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.getDigitalPrintOrderById') ")
    public DigitalPrintOrder getDigitalPrintOrderById(String id) {
        return orderManagerService.getDigitalPrintOrderById(id);
    }

    @PostMapping("/setPrintOrderStatus")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.setPrintOrderStatus') and " +
            "@orderRepository.existsById(#orderIdAndStatus.orderId) ")
    public DigitalPrintOrder setPrintOrderStatus(@RequestBody OrderIdAndStatus orderIdAndStatus) {
        DigitalPrintOrder order = (DigitalPrintOrder) orderService.getOrderById(orderIdAndStatus.getOrderId());
        order.setStatus(orderIdAndStatus.getOrderStatus());
        orderService.writeOrder(order);
        return order;
    }

    @GetMapping("/getPage")
    @PreAuthorize("@securityService.hasPattern(principal, 'manager.getPage') ")
    public List<Order> getPage(Integer page,
	                             Integer size,
	                             @RequestParam(value = "sorts[]", required = false)
		                                 String[] sorts,
	                             @RequestParam(value = "filters[]", required = false)
		                                 String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
        return orderService.readPage(page, size, _filters, _sorts);
    }

//    @GetMapping("/getPagedDesignOrders")
//    @PreAuthorize("@securityService.hasPattern(principal, 'manager.getPagedDesignOrders') ")
//    public List<DesignOrder> getPagedDesignOrders(Integer page,
//                                                  Integer size,
//                                                  String sortBy,
//                                                  Sort.Direction sortDirection,
//                                                  String filterBy,
//                                                  String filter) {
//        return orderManagerService.readPageDesignOrders(page, size, sortBy, sortDirection, filterBy, filter);
//    }
//
//    @GetMapping("/getPagedDigitalPrintOrders")
//    @PreAuthorize("@securityService.hasPattern(principal, 'manager.getPagedDigitalPrintOrders') ")
//    public List<DigitalPrintOrder> getPagedDigitalPrintOrders(Integer page,
//                                                              Integer size,
//                                                              String sortBy,
//                                                              Sort.Direction sortDirection,
//                                                              String filterBy,
//                                                              String filter) {
//        return orderManagerService.readPageDigitalPrintOrders(page, size, sortBy, sortDirection, filterBy, filter);
//    }
//
//    @GetMapping("/getPagedOrders")
//    @PreAuthorize("@securityService.hasPattern(principal, 'manager.getPagedOrders') ")
//    public List<Order> getPagedOrders(Integer page,
//                                      Integer size,
//                                      String sortBy,
//                                      Sort.Direction sortDirection,
//                                      String filterBy,
//                                      String filter) {
//        return orderService.readPageOrders(page, size, sortBy, sortDirection, filterBy, filter);
//    }
}
