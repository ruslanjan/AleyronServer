package com.aleyron.services.digital_print.services.storage.controllers;

import com.aleyron.services.digital_print.services.storage.documents.StorageOperation;
import com.aleyron.services.digital_print.services.storage.logic.StorageHistoryService;
import com.aleyron.utils.queryhelpers.filter.FilterInput;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.SortInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Deprecated
//@RestController
@Component
@RequestMapping("/api/digital_print/storage/history")
public class StorageHistoryController {
    private StorageHistoryService historyService;

    @Autowired
    public StorageHistoryController(StorageHistoryService historyService) {
        this.historyService = historyService;
    }

    @GetMapping("/getOperation")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.history.get')")
    public StorageOperation getOperation(String id) {
        return historyService.getOperation(id);
    }

    @GetMapping("/getAll")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.history.get')")
    public List<StorageOperation> getAll() {
        return historyService.getAll();
    }

    @GetMapping("/getPage")
    @PreAuthorize("@securityService.hasPattern(principal, 'digital_print.storage.history.get')")
    public List<StorageOperation> getPage(Integer page,
	                             Integer size,
	                             @RequestParam(value = "sorts[]", required = false)
		                                 String[] sorts,
	                             @RequestParam(value = "filters[]", required = false)
		                                 String[] filters
	) {
		List < Sort > _sorts = SortInput.parse(sorts);
		List < FilterRequest > _filters = FilterInput.parse(filters);
        return historyService.readPage(page, size, _filters, _sorts);
    }
}
