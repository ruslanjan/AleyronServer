package com.aleyron.services.digital_print.services.storage.repositories;

import com.aleyron.services.digital_print.services.storage.documents.StorageOperation;
import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Map;

@Deprecated
public interface StorageOperationRepository extends MongoRepository<StorageOperation, String> {
    StorageOperation getById(String id);

    @Query("?0")
Page<StorageOperation> getFilteredPage(Map< String, FilterOperation > filters, Pageable pageable);
}
