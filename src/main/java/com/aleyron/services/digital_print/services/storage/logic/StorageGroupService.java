package com.aleyron.services.digital_print.services.storage.logic;

import com.aleyron.services.digital_print.services.storage.documents.StorageGroup;
import com.aleyron.services.digital_print.services.storage.repositories.StorageGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Deprecated
@Service
public class StorageGroupService {

	private StorageGroupRepository groupRepository;

	@Autowired
	public StorageGroupService(StorageGroupRepository groupRepository) {
		this.groupRepository = groupRepository;
	}

	public List < StorageGroup > readAll( ) {
		return groupRepository.findAll();
	}

	public StorageGroup create(StorageGroup group) {
		return groupRepository.save(group);
	}

	public List < StorageGroup > readPage(Integer page, Integer size) {
		return readPage(page, size, null, null, null, null);
	}

	public List < StorageGroup > readPage(Integer page, Integer size, String sortBy, Sort.Direction sortDirection, String filterBy, String filter) {
		return Collections.emptyList();
	}

	public void delete(String id) {
		groupRepository.deleteById(id);
	}

	public StorageGroup read(String id) {
		return groupRepository.getById(id);
	}
}
