package com.aleyron.services.digital_print.services.storage.data;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/29/18
 */
public enum MetricUnitEnum {
    MILLILITER, PIECE, METER, SQUARE_METER, GRAMME
}
