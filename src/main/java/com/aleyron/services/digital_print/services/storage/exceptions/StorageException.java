package com.aleyron.services.digital_print.services.storage.exceptions;

@SuppressWarnings("WeakerAccess")
public abstract class StorageException extends Exception {
}
