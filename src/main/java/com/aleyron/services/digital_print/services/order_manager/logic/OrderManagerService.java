package com.aleyron.services.digital_print.services.order_manager.logic;

import com.aleyron.services.digital_print.services.storage.logic.StorageItemService;
import com.aleyron.services.order.data.OrderType;
import com.aleyron.services.order.data.PrintOrderStatus;
import com.aleyron.services.order.documents.DesignOrder;
import com.aleyron.services.order.documents.DigitalPrintOrder;
import com.aleyron.services.order.logic.OrderService;
import com.aleyron.services.order.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 6/23/18
 */
@Service
public class OrderManagerService {
    private OrderService orderService;
    private OrderRepository orderRepository;
    private StorageItemService digitalStorageService;


    @Autowired
    public OrderManagerService(OrderService orderService, OrderRepository orderRepository, StorageItemService digitalStorageService) {
        this.orderService = orderService;
        this.orderRepository = orderRepository;
        this.digitalStorageService = digitalStorageService;
    }

    public DigitalPrintOrder getDigitalPrintOrderById(String id) {
        return (DigitalPrintOrder) orderRepository.getById(id);
    }

    public DesignOrder getDesignOrderById(String id) {
        return (DesignOrder) orderRepository.getById(id);
    }

    public List<DesignOrder> getPagedDesignOrders(int page, int size) {
        return (List<DesignOrder>) (Object) orderRepository.findAllByType(OrderType.DESIGN, PageRequest.of(page, size)).getContent();
    }

    public List<DigitalPrintOrder> getPagedDigitalPrintOrders(int page, int size) {
        return (List<DigitalPrintOrder>) (Object) orderRepository.findAllByType(OrderType.WIDE_FORMAT_PRINT, PageRequest.of(page, size)).getContent();
    }

    public DigitalPrintOrder startWorkingOnOrder(String id) throws Exception {
        DigitalPrintOrder order = (DigitalPrintOrder) orderRepository.getById(id);
        if (order.getStatus() != PrintOrderStatus.WAITING)
            throw new Exception("Order is not waiting");
        order.setStatus(PrintOrderStatus.PROCESSING);
        return order;
    }

    public DigitalPrintOrder finishOrder(String id) {
        DigitalPrintOrder order = (DigitalPrintOrder) orderRepository.getById(id);
        order.setStatus(PrintOrderStatus.FINISHED);
        return order;
    }
}
