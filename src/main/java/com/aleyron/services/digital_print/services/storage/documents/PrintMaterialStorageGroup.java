package com.aleyron.services.digital_print.services.storage.documents;

import com.aleyron.services.digital_print.services.storage.data.MetricUnitEnum;
import com.aleyron.services.digital_print.services.storage.data.StorageGroupType;
import com.aleyron.services.digital_print.services.storage.exceptions.InvalidUnitsException;

import java.util.List;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/3/18
 */
@Deprecated
public class PrintMaterialStorageGroup extends StorageGroup {
    private double quantity;
    private double cost;
    private MetricUnitEnum unit = MetricUnitEnum.SQUARE_METER;

    public PrintMaterialStorageGroup() {
        setType(StorageGroupType.PRINT_MATERIAL_GROUP);
    }

    @Override
    public StorageGroup setStorageItemList(List<StorageItem> storageItemList) throws InvalidUnitsException {
        for (StorageItem item : storageItemList) {
            if (item.getUnit() != MetricUnitEnum.PIECE) {
                throw new InvalidUnitsException();
            }
            quantity += item.getHeight() * item.getWidth() * item.getQuantity();
            cost += item.getCost();
        }
        return super.setStorageItemList(storageItemList);
    }

    public double getQuantity() {
        return quantity;
    }

    public PrintMaterialStorageGroup setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    public double getCost() {
        return cost;
    }

    public PrintMaterialStorageGroup setCost(double cost) {
        this.cost = cost;
        return this;
    }

    public MetricUnitEnum getUnit() {
        return unit;
    }

    public PrintMaterialStorageGroup setUnit(MetricUnitEnum unit) {
        this.unit = unit;
        return this;
    }
}
