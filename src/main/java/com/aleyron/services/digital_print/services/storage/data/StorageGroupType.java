package com.aleyron.services.digital_print.services.storage.data;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/3/18
 */
@Deprecated
public enum StorageGroupType {
    PRINT_MATERIAL_GROUP, BASIC_GROUP

}
