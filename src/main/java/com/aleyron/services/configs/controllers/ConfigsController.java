package com.aleyron.services.configs.controllers;

import com.aleyron.services.configs.documents.PriceCalculationConfig;
import com.aleyron.services.configs.services.ConfigsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/4/18
 */
@RestController
@RequestMapping("/api/configs")
public class ConfigsController {

    private ConfigsService configsService;

    public ConfigsController(ConfigsService configsService) {
        this.configsService = configsService;
    }


    @GetMapping("/getPriceCalculationConfig")
    @PreAuthorize("@securityService.hasPattern(principal, 'configs.getPriceCalculationConfig.items.getPriceCalculationConfig')")
    public PriceCalculationConfig getPriceCalculationConfig() {
        return configsService.getPriceCalculationConfig();
    }

    @PostMapping("/savePriceCalculationConfig")
    @PreAuthorize("@securityService.hasPattern(principal, 'configs.getPriceCalculationConfig.items.savePriceCalculationConfig')")
    public PriceCalculationConfig savePriceCalculationConfig(@RequestBody PriceCalculationConfig priceCalculationConfig) {
        return configsService.setPriceCalculationConfig(priceCalculationConfig);
    }
}
