package com.aleyron.services.configs.repositories;

import com.aleyron.services.configs.documents.Config;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/4/18
 */
public interface ConfigRepository extends MongoRepository<Config, String> {
    Config getByName(String name);
}
