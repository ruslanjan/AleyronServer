package com.aleyron.services.configs.documents;

import com.aleyron.services.order.data.OrderType;

import java.util.Map;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/4/18
 */
public class PriceCalculationConfig extends Config {

    private Map<OrderType, Double> additionPricePerSquareMeter;

    public PriceCalculationConfig() {
        setName("PriceCalculationConfig");
    }

    public Map<OrderType, Double> getAdditionPricePerSquareMeter() {
        return additionPricePerSquareMeter;
    }

    public PriceCalculationConfig setAdditionPricePerSquareMeter(Map<OrderType, Double> additionPricePerSquareMeter) {
        this.additionPricePerSquareMeter = additionPricePerSquareMeter;
        return this;
    }
}
