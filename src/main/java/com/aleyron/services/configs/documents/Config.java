package com.aleyron.services.configs.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/4/18
 */
@Document
public class Config {
    @Id
    String id;

    @Indexed(unique = true, sparse = true)
    String name;

    public String getId() {
        return id;
    }

    public Config setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Config setName(String name) {
        this.name = name;
        return this;
    }
}
