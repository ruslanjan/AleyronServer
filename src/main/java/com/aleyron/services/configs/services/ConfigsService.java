package com.aleyron.services.configs.services;

import com.aleyron.services.configs.documents.PriceCalculationConfig;
import com.aleyron.services.configs.repositories.ConfigRepository;
import com.aleyron.services.order.data.OrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/4/18
 */
@Service
public class ConfigsService {
    private ConfigRepository configRepository;

    @Autowired
    public ConfigsService(ConfigRepository configRepository) {
        this.configRepository = configRepository;
        Map<OrderType, Double> map = new HashMap<>();
        map.put(OrderType.WIDE_FORMAT_PRINT, .0);
        map.put(OrderType.DIGITAL_PRINT, .0);
        map.put(OrderType.STICKER, .0);
        try {
            configRepository.save(new PriceCalculationConfig().setAdditionPricePerSquareMeter(map));
        } catch (Exception ignored) {
        }
    }

    public PriceCalculationConfig getPriceCalculationConfig() {
        return (PriceCalculationConfig) configRepository.getByName("PriceCalculationConfig");
    }

    public PriceCalculationConfig setPriceCalculationConfig(PriceCalculationConfig priceCalculationConfig) {
        return configRepository.save(priceCalculationConfig);
    }

}
