package com.aleyron.utils.queryhelpers.pagination;

import com.aleyron.utils.queryhelpers.filter.FilterOperation;
import com.aleyron.utils.queryhelpers.filter.FilterRequest;
import com.aleyron.utils.queryhelpers.filter.FilteredPageRequest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Page request generator
 *
 * This class contains static methods which can help you generate MongoDB-ready pagination requests from HTTP GET request.
 *
 * @author Karl Meinkopf
 */
public final class PageRequestGenerator {
	/**
	 * Locked constructor. Does nothing.
	 */
	private PageRequestGenerator( ) {
	}

	/**
	 * Generates page request with multiple sorting and filtering.
	 *
	 * @param page Page number, starting from 0
	 * @param size Page size
	 * @param sorts Array of Sorts to apply to the request
	 * @param filters Array of filter request to apply to the request
	 * @return generated request
	 * @see FilteredPageRequest
	 */
	public static FilteredPageRequest getPageFilteredSorted(Integer page, Integer size, List < Sort > sorts, List < FilterRequest > filters) {
		Pageable pageable = PageRequest.of(page, size);
		if (sorts.size() > 0) {
			Sort sort = null;
			for (Sort _sort :
				sorts) {
				sort = (sort == null ?
					_sort :
					sort.and(_sort));
			}

			pageable = PageRequest.of(page, size, sort);
		}

		Map < String, FilterOperation > _filters = new HashMap <>();
		for (FilterRequest sefr :
			filters) {
			_filters.put(sefr.getField(), sefr.getOperation());
		}

		return FilteredPageRequest.of(_filters, pageable);
	}
}
