package com.aleyron.utils.queryhelpers.filter;

import java.util.*;

/**
 * Structure that contains MongoDB search operator and value.
 * For example,
 * <pre><code>
 * new FilterOperation &lt; String &gt; (FilterType.$eq, "foobar")
 * </code></pre>
 * will be transformed to MongoDB's <code>{"$eq": "foobar"}</code>
 *
 * @param <T> filter value type
 * @author Karl Meinkopf
 */
public class FilterOperation < T > implements Map < FilterType, T > {

	private final FilterType operation;
	private final T value;

	FilterOperation(FilterType operation, T value) {
		this.operation = operation;
		this.value = value;
	}

	@Override
	public int size( ) {
		return 1;
	}

	@Override
	public boolean isEmpty( ) {
		return false;
	}

	@Override
	public boolean containsKey(Object key) {
		return (FilterType.valueOf(key.toString()) == (operation));
	}

	@Override
	public boolean containsValue(Object value) {
		return (value.toString()) == (this.value);
	}

	@Override
	public T get(Object key) {
		return value;
	}

	@Override
	public T put(FilterType key, T value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public T remove(Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(Map m) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear( ) {

	}

	@Override
	public Set < FilterType > keySet( ) {
		return new HashSet <>(Collections.singletonList(operation));
	}

	@Override
	public Collection < T > values( ) {
		return (Collections.singletonList(value));
	}

	@Override
	public Set < Map.Entry < FilterType, T > > entrySet( ) {
		return new HashSet <>(Collections.singletonList(new Entry(operation, value)));
	}

	class Entry implements Map.Entry < FilterType, T > {

		private final FilterType key;
		private final T value;

		Entry(FilterType key, T value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public FilterType getKey( ) {
			return key;
		}

		@Override
		public T getValue( ) {
			return value;
		}

		@Override
		public T setValue(T value) {
			throw new UnsupportedOperationException();
		}
	}
}
