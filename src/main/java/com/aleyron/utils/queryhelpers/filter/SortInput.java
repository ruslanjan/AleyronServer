package com.aleyron.utils.queryhelpers.filter;

import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Sort request parser.
 * <p>
 * Any paged requests are basically made with HTTP GET, which does not support transfering objects, only arrays.
 * FilterInput string looks like this: <code>field:DIRECTION</code>
 * DIRECTION may be ASC or DESC.
 * </p>
 *
 * @author Karl Meinkopf
 */
public class SortInput {
	private final String input;

	private SortInput(String sortInput) {
		input = sortInput;
	}

	/**
	 * Performs parsing (see class description)
	 *
	 * @param input input string
	 * @return parsed sort request
	 */
	public static Sort parse(String input) {
		return (new SortInput(input)).getSort();
	}

	public static List < Sort > parse(String[] inputs) {
		List < Sort > _sorts = new ArrayList <>();
		if(inputs == null) return Collections.emptyList();
		for (String sort :
			inputs) {
			_sorts.add(SortInput.parse(sort));
		}
		return _sorts;
	}

	private Sort getSort( ) throws IllegalArgumentException {
		try {
			String[] pieces = input.split(":");
			String field = pieces[0];
			Sort.Direction direction = Sort.Direction.fromString(pieces[1]);
			return Sort.by(direction, field);
		} catch (Exception e) {
			throw new IllegalArgumentException("failed to parse input!", e);
		}
	}
}
