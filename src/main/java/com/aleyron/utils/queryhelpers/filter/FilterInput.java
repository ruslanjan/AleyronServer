package com.aleyron.utils.queryhelpers.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Filter request parser.
 * <p>
 * Any paged requests are basically made with HTTP GET, which does not support transfering objects, only arrays.
 * FilterInput string looks like this: <pre><code>field=$operation|search value</code></pre>
 * While 'filter' and 'value' are clear, $operation needs to be described.
 * $operation is an enum {@link FilterType} of MongoDB search queries.
 *
 * @author Karl Meinkopf
 */
public class FilterInput {

	final private String input;

	private FilterInput(String sortInput) {
		input = sortInput;
	}

	/**
	 * Performs parsing (see class description)
	 *
	 * @param input string to parse
	 * @return parsed filter object
	 */
	public static FilterRequest parse(String input) {
		return (new FilterInput(input)).getRequest();
	}

	public static List < FilterRequest > parse(String[] inputs) {
		List < FilterRequest > _filters = new ArrayList <>();
		if(inputs == null) return Collections.emptyList();
		for (String filter :
			inputs) {
			_filters.add(FilterInput.parse(filter));
		}
		return _filters;
	}

	private FilterRequest getRequest( ) throws IllegalArgumentException {
		try {
			String[] pieces = input.split("=");
			String field = pieces[0];
			String value = pieces[1];
			pieces = value.split("\\|");
			FilterType operation = FilterType.valueOf(pieces[0]);
			value = pieces[1];
			return new FilterRequest <>(field, operation, value);
		} catch (Exception e) {
			throw new IllegalArgumentException("failed to parse input!", e);
		}
	}

}
