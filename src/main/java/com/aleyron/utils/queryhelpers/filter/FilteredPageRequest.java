package com.aleyron.utils.queryhelpers.filter;

import com.aleyron.utils.queryhelpers.pagination.PageRequestGenerator;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * FilteredPageRequest struct.
 *
 * Contains Spring Boot PageRequest and a map of filters.
 * <p>Recommended repository method looks like this: </p>
 * <pre><code>
 *     &#x40;Query("?0")
 *     public Page &lt; Document &gt; getPageFilteredSorted(Map &lt; String, FilterOperation &gt; filters, Pageable pageable);
 * </code></pre>
 *
 * @author Karl Meinkopf
 * @see PageRequestGenerator
 */
public class FilteredPageRequest {
	private Pageable pageable;
	private Map < String, FilterOperation > filters;

	/**
	 * Default constructor.
	 * To get an instance, use {@link FilteredPageRequest.of} instead
	 *
	 * @param pageable A pageable, formerly PageRequest object
	 * @param filters Map of {@link String} -> {@link FilterOperation}
	 */
	private FilteredPageRequest(Pageable pageable, Map < String, FilterOperation > filters) {
		this.pageable = pageable;
		this.filters = filters;
	}

	/**
	 * See constructor for details
	 *
	 * @param filters filters
	 * @param pageable pageable
	 * @return a new instance of FilteredPageRequest
	 */
	static public FilteredPageRequest of(Map < String, FilterOperation > filters, Pageable pageable) {
		return new FilteredPageRequest(pageable, filters);
	}

	/**
	 * @return Pageable object
	 */
	public Pageable getPageable( ) {
		return pageable;
	}

	/**
	 * Fluent setter
	 * @param pageable new Pageable
	 * @return self
	 */
	public FilteredPageRequest setPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}

	/**
	 * @return filters map
	 */
	public Map < String, FilterOperation > getFilters( ) {
		return filters;
	}

	/**
	 * Fluent setter
	 * @param filters new filters map
	 * @return self
	 */
	public FilteredPageRequest setFilters(Map < String, FilterOperation > filters) {
		this.filters = filters;
		return this;
	}
}
