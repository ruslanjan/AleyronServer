package com.aleyron.utils.queryhelpers.filter;

/**
 * Struct that contains parsed filter.
 *
 * @see FilterInput
 * @param <T> {@link FilterOperation} value type
 */
public class FilterRequest < T > {
	private String field;
	private FilterOperation < T > operation;

	FilterRequest(String field, FilterType operation, T value) {
		this.field = field;
		this.operation = new FilterOperation <>(operation, value);
	}

	public String getField( ) {
		return field;
	}

	public FilterRequest setField(String field) {
		this.field = field;
		return this;
	}

	public FilterOperation < T > getOperation( ) {
		return operation;
	}

	public FilterRequest setOperation(FilterOperation < T > operation) {
		this.operation = operation;
		return this;
	}
}
