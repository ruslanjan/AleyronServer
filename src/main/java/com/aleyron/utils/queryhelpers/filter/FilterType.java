package com.aleyron.utils.queryhelpers.filter;

/**
 * Allowed MongoDB search operations.
 * @author Karl Meinkopf
 */
public enum FilterType {
	$eq, $search
}
