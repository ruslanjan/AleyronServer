/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

package com.aleyron;

import com.aleyron.services.booker.logic.BookerService;
import com.aleyron.services.users.documents.AleyronUser;
import com.aleyron.services.users.documents.Privilege;
import com.aleyron.services.users.repositories.AleyronUserRepository;
import com.aleyron.services.users.repositories.PrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom randomGenerator = new SecureRandom();
    private final AleyronUserRepository aleyronUserRepository;
    private final PrivilegeRepository privilegeRepository;

    @Autowired
    public Application(AleyronUserRepository aleyronUserRepository, PrivilegeRepository privilegeRepository, BookerService bookerService) {
        this.aleyronUserRepository = aleyronUserRepository;
        this.privilegeRepository = privilegeRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Админ по умолчанию
     *
     * @param args аргс
     */
    @Override
    public void run(String... args) {
        Privilege admin = new Privilege().setName("admin").setPattern(".*");
        if (!privilegeRepository.existsByPattern(".*"))
            admin = privilegeRepository.save(admin);
        else
            admin = privilegeRepository.getByPattern(".*");

        AleyronUser aleyronUser = new AleyronUser();
        if (aleyronUserRepository.existsByUsername("admin"))
            aleyronUser = aleyronUserRepository.getByUsername("admin");
        String password = randomString(4);
        aleyronUser.setUsername("admin");
        aleyronUser.setPassword(password);
        System.out.println("admin password: " + password);
        aleyronUser.setRoles(new ArrayList<>());
        aleyronUser.setPrivileges(Collections.singletonList(admin));
        aleyronUserRepository.save(aleyronUser);
    }

    private String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(randomGenerator.nextInt(AB.length())));
        return sb.toString();
    }
}
