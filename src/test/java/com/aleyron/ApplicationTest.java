///*
// * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
// * Unauthorized copying of this file is strictly prohibited
// * Proprietary and confidential
// */
//
//package com.aleyron;
//
//import org.junit.Ignore;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//public class ApplicationTest {
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Test
//    public void mod() throws Exception {
//        this.mockMvc.perform(get("/mod"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().string("Tomorrow never dies"));
//    }
//
//    @Ignore
//    @Test
//    public void indexLoads() throws Exception {
//        this.mockMvc.perform(get("/"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentType("text/html"));
//    }
//
//    @Test
//    public void contextLoads() {
//    }
//}