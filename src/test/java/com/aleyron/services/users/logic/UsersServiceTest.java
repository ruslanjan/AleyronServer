///*
// * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
// * Unauthorized copying of this file is strictly prohibited
// * Proprietary and confidential
// */
//
//package com.aleyron.services.users.logic;
//
//import com.aleyron.services.users.documents.AleyronUser;
//import com.aleyron.services.users.repositories.AleyronUserRepository;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageImpl;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.Arrays;
//import java.util.Base64;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class UsersServiceTest {
//
//	private final String username = "user";
//	private final String password = "user";
//	private final String authorization = "Basic" + new String(Base64.getEncoder().encode((username + ":" + password).getBytes()));
//
//	@MockBean
//	AleyronUserRepository aleyronUserRepository;
//
//	@Autowired
//	UsersService usersService;
//
//	@Test
//	public void writeUser() {
//	}
//
//	@Test
//	public void deleteById() {
//	}
//
//	@Test
//	public void getPage() {
//		AleyronUser[] users = new AleyronUser[]{new AleyronUser().setUsername("Alpha")};
//		Page<AleyronUser> userPage = new PageImpl<>(Arrays.asList(users));
//		when(aleyronUserRepository.findAll(any(PageRequest.class)))
//				.thenReturn(userPage);
//		Assert.assertArrayEquals(usersService.getAleyronUsersPage(0, 12).toArray(), users);
//	}
//
//	@Test
//	public void findByUsername() {
//	}
//}