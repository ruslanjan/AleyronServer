///*
// * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
// * Unauthorized copying of this file is strictly prohibited
// * Proprietary and confidential
// */
//
//package com.aleyron.services.users.controllers;
//
//import com.aleyron.services.users.documents.AleyronUser;
//import com.aleyron.services.users.logic.UsersService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.stubbing.Answer;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.util.Base64;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.mockito.Mockito.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//public class UsersControllerTest {
//
//	private final String username = "user";
//	private final String password = "user";
//	private final String authorization = "Basic" + new String(Base64.getEncoder().encode((username + ":" + password).getBytes()));
//
//	@Autowired
//	private MockMvc mockMvc;
//
//	@MockBean
//	private UsersService mockUsersService;
//
//	@Test
//	@WithMockUser(username = username, password = password, roles = {"ADMIN"})
//	public void registerUser_WithoutUsernameAndPassword() throws Exception {
//		AleyronUser user = new AleyronUser();
//
//		mockMvc.perform(
//				post("/api/users/registerUser")
//						.contentType(MediaType.APPLICATION_JSON)
//						.header("Authorization", authorization)
//						.accept(MediaType.APPLICATION_JSON)
//						.content(new ObjectMapper().writeValueAsString(user)))
//				.andDo(print())
//				.andExpect(status().isBadRequest());
//	}
//
////	@Test
////	@WithMockUser(username = username, password = password, roles = {"ADMIN"})
////	public void registerUser() throws Exception {
////		AleyronUserInput user = new AleyronUserInput().setUsername(username).setPassword(password);
////		String mockId = "Just Read the Instructions";
////
////		ArgumentCaptor<AleyronUser> captor = ArgumentCaptor.forClass(AleyronUser.class);
////
////		when(mockUsersService.saveUser(captor.capture()))
////				.thenAnswer((Answer<AleyronUser>) invocation -> ((AleyronUser) invocation.getArguments()[0]).setId(mockId));
////		mockMvc.perform(
////				post("/api/users/registerUser")
////						.contentType(MediaType.APPLICATION_JSON)
////						.header("Authorization", authorization)
////						.accept(MediaType.APPLICATION_JSON)
////						.content(new ObjectMapper().writeValueAsString(user)))
////				.andDo(print())
////				.andExpect(status().isOk())
////				.andExpect(content().string(new ObjectMapper().writeValueAsString(user.toAleyronUser().setId(mockId))));
////		verify(mockUsersService, times(1)).saveUser(any(AleyronUser.class));
////		Assert.assertEquals(user.username, captor.getValue().getUsername());
////		Assert.assertEquals(mockId, captor.getValue().getId());
////	}
//
//	@Test
//	@WithMockUser(username = username, password = password, roles = {"ADMIN"})
//	public void deleteUserById() throws Exception {
//		AleyronUser user = new AleyronUser().setUsername("user");
//		String mockId = "Just Read the Instructions";
//		user.setId(mockId);
//
//		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
//
//		when(mockUsersService.deleteAleyronUserById(captor.capture()))
//				.thenAnswer((Answer<AleyronUser>) invocation -> user);
//
//		mockMvc.perform(
//				post("/api/users/deleteUserById")
//						.contentType(MediaType.APPLICATION_JSON)
//						.header("Authorization", authorization)
//						.accept(MediaType.APPLICATION_JSON)
//						.content(mockId))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andExpect(content().string(new ObjectMapper().writeValueAsString(user)));
//		verify(mockUsersService, times(1)).deleteAleyronUserById(anyString());
//		Assert.assertEquals(mockId, captor.getValue());
//	}
//
////	@Test
////
////	public void getAleyronUsersPage() {
////
////	}
////
////	@Test
////	public void findByUsername() {
////		AleyronUser user = new AleyronUser().setUsername(username);
////		String mockId = "Just Read the Instructions";
////
////	}
//}