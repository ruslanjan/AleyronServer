package com.aleyron.services.manager.logic;

import com.aleyron.services.booker.documents.Invoice;
import com.aleyron.services.booker.logic.BookerService;
import com.aleyron.services.order.documents.Order;
import com.aleyron.services.order.logic.OrderService;
import com.aleyron.services.order.repositories.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Copyright (C) Ruslan Jankurazov - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ruslan Jankurazov <ruslanjan@gmail.com>, 7/20/18
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ManagerServiceTest {


    @MockBean
    private OrderService orderService;
    @MockBean
    private BookerService bookerService;
    @MockBean
    private OrderRepository orderRepository;

    @Autowired
    ManagerService managerService;

    @Test
    public void deleteOrderById() throws Exception {
        //Prepare data
        Random random = new Random();
        String invoiceId = "invoiceMockId" + Integer.toString(random.nextInt());
        String orderId = "orderMockId" + Integer.toString(random.nextInt());

        Invoice invoice = new Invoice().setId(invoiceId);
        Order order = new Order().setId(orderId);

        invoice.getOrderIds().add(orderId);
        order.setInvoice(invoice);
        //testing
        when(orderService.getOrderById(orderId))
                .thenReturn(order);
        when(bookerService.getInvoiceById(orderId))
                .thenReturn(invoice);

        ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
        ArgumentCaptor<Order> orderArgumentCaptor = ArgumentCaptor.forClass(Order.class);
        ArgumentCaptor<String> orderIdArgumentCaptor = ArgumentCaptor.forClass(String.class);

        when(bookerService.writeInvoice(invoiceArgumentCaptor.capture()))
                .thenAnswer((Answer<Invoice>) invocation -> invocation.getArgument(0));
        when(orderService.writeOrder(orderArgumentCaptor.capture()))
                .thenAnswer((Answer<Order>) invocation -> invocation.getArgument(0));
        when(orderService.deleteById(orderIdArgumentCaptor.capture()))
                .thenAnswer((Answer<Order>) invocation -> orderArgumentCaptor.getValue());

        managerService.deleteOrderById(orderId);
        assertNull(orderArgumentCaptor.getValue().getInvoice());
        assertTrue(!invoiceArgumentCaptor.getValue().getOrderIds().contains(orderId));
    }
}