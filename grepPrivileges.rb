require 'pp'
require 'json'

files = Dir['**/*.java']

def parse(strings)
	regex   = /hasPattern\(principal,[\s]*['"](.*?)['"]/
	matches = strings.scan regex
end

matches = []

IO.write 'privileges.json', '{}' unless File.exists? 'privileges.json'

files.each do |file|
	strings = IO.read file
	matches += parse(strings)
end

matches = matches.map(&:first).uniq
object  = JSON.parse(IO.read('privileges.json')) || {}

matches.each do |priv|
	if(object[priv].to_s.length <= 0)
		object[priv] = ''
	end
end

IO.write 'privileges.json', JSON.pretty_unparse(object)